#include <stdio.h>

void hanoi(int n, char torg, char tdst, char taux){
	if(n == 1){
		printf("\nmove o disco %d da torre %c para a torre %c", n, torg, tdst);
		return;
	}
	hanoi(n-1, torg, taux, tdst);
	printf("\nmove o disco %d da torre %c para a torre %c", n, torg, tdst);
	hanoi(n-1, taux, tdst, torg);
	return;
}

int main(int argc, char const *argv[])
{
	hanoi(5, 'A', 'C', 'B');
	printf("\n");
	return 0;
}