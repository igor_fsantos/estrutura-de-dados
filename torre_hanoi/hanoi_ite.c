#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int *aloca_vet_torre(int n){
	int *vet = (int *)calloc(n, sizeof(int));
	if(!vet){
		printf("[erro]: Nao foi possivel alocar memoria\n");
		exit(1);
	}
	return vet;
}

void move_disco(char t1, char t2, int *vet_t1, int *vet_t2, int *topo_t1, int *topo_t2){
	if(*topo_t2 == -1){
		printf("\nmove o disco %d da torre %c para a torre %c", vet_t1[*topo_t1], t1, t2);
		*topo_t2 = *topo_t2 + 1;
		vet_t2[*topo_t2] = vet_t1[*topo_t1];
		*topo_t1 = *topo_t1 - 1;
	}
	else if(*topo_t1 == -1){
		printf("\nmove o disco %d da torre %c para a torre %c", vet_t2[*topo_t2], t2, t1);
		*topo_t1 = *topo_t1 + 1;
		vet_t1[*topo_t1] = vet_t2[*topo_t2];
		*topo_t2 = *topo_t2 - 1;
	}
	else if(vet_t1[*topo_t1] < vet_t2[*topo_t2]){
		printf("\nmove o disco %d da torre %c para a torre %c", vet_t1[*topo_t1], t1, t2);
		*topo_t2 = *topo_t2 + 1;
		vet_t2[*topo_t2] = vet_t1[*topo_t1];
		*topo_t1 = *topo_t1 - 1;
	}
	else{
		printf("\nmove o disco %d da torre %c para a torre %c", vet_t2[*topo_t2], t2, t1);
		*topo_t1 = *topo_t1 + 1;
		vet_t1[*topo_t1] = vet_t2[*topo_t2];
		*topo_t2 = *topo_t2 - 1;
	}
	return;
}

void hanoi_ite(int n, char torg, char tdst, char taux){
	int *vet_torg, *vet_tdst, *vet_taux, topo_torg, topo_tdst, topo_taux, i;
	vet_torg = aloca_vet_torre(n);
	vet_tdst = aloca_vet_torre(n);
	vet_taux = aloca_vet_torre(n);
	topo_torg = topo_tdst = topo_taux = -1;
	for(i = n; i >= 1; i--){
		topo_torg++;
		vet_torg[topo_torg] = i;
	}
	if(n % 2 == 0){
		while(topo_tdst < n-1){
			move_disco(torg, taux, vet_torg, vet_taux, &topo_torg, &topo_taux);
			move_disco(torg, tdst, vet_torg, vet_tdst, &topo_torg, &topo_tdst);
			move_disco(taux, tdst, vet_taux, vet_tdst, &topo_taux, &topo_tdst);
		}
	}
	else{
		while(topo_tdst < n-1){
			move_disco(torg, tdst, vet_torg, vet_tdst, &topo_torg, &topo_tdst);
			if(topo_tdst == n-1) break;
			move_disco(torg, taux, vet_torg, vet_taux, &topo_torg, &topo_taux);
			move_disco(tdst, taux, vet_tdst, vet_taux, &topo_tdst, &topo_taux);
		}
	}
	printf("\n");
	free(vet_torg);
	free(vet_tdst);
	free(vet_taux);
	return;
}

int main(int argc, char const *argv[])
{
	if(argc != 2){
		printf("[uso]: %s <numero de discos>\n", argv[0]);
		return 0;
	}
	hanoi_ite(atoi(argv[1]), 'A', 'C', 'B');
	return 0;
}