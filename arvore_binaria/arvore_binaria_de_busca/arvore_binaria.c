#include "funcoes_arvore.h"

int main(int argc, char const *argv[])
{
	ab_t *arvore = NULL;
	int escolha, valor;
	while(V){
		menu();
		scanf("%d", &escolha);
		if(escolha == 1){
			printf("Entre com o valor a ser inserido: ");
			scanf("%d", &valor);
			arvore = insere(arvore, valor);
			printf("\n");
		}
		else if(escolha == 2){
			printf("Entre com o valor a ser removido: ");
			scanf("%d", &valor);
			arvore = removeNo(arvore, valor);
			printf("\n");
		}
		else if(escolha == 3){
			printf("\nImpressao:\n");
			if(!arvore) printf("A arvore esta' vazia");
			imprime_pre_ordem(arvore);
			printf("\n\n");
		}
		else if(escolha == 4){
			printf("\nImpressao:\n");
			if(!arvore) printf("A arvore esta' vazia");
			imprime_pos_ordem(arvore);
			printf("\n\n");
		}
		else if(escolha == 5){
			 printf("\nImpressao:\n");
			 if(!arvore) printf("A arvore esta' vazia");
			 imprime_em_ordem(arvore);
			 printf("\n\n");
		}
		else if(escolha == -1) break;
		else printf("\nEntrada invalida\n\n");
	}
	return 0;
}