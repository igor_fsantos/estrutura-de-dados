#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct ab_t{
	int k;
	struct ab_t *esq, *dir;
};

typedef struct ab_t ab_t;

#define V 1
#define F 0

#define GRAU0 0
#define GRAU1 1
#define GRAU2 2

#ifndef _FUNCOES_ARVORE_H
#define _FUNCOES_ARVORE_H

ab_t *cria_no(int k);
ab_t *insere(ab_t *r, int k);
void imprime_pre_ordem(ab_t *r);
void imprime_pos_ordem(ab_t *r);
void imprime_em_ordem(ab_t *r);
void menu(void);
ab_t *busca(ab_t *r, int k);
int ehFolha(ab_t *r);
ab_t *retornaPai(ab_t *r, ab_t *no);
ab_t *removeNo(ab_t *r, int k);
int grauDoNo(ab_t *no);
ab_t *maisADireita(ab_t *no);

#endif