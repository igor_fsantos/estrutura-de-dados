#include "funcoes_arvore.h"

ab_t *cria_no(int k){
	ab_t *no = (ab_t *)calloc(1, sizeof(ab_t));
	if(!no) return NULL;
	no->k = k;
	no->esq = NULL;
	no->dir = NULL;
	return no;
}

ab_t *insere(ab_t *r, int k){
	if(r == NULL) r = cria_no(k);
	else if(k <= r->k) r->esq = insere(r->esq, k);
	else r->dir = insere(r->dir, k);
	return r;
}

void imprime_pre_ordem(ab_t *r){
	if(r == NULL) return;
	printf("%d ", r->k);
	imprime_pre_ordem(r->esq);
	imprime_pre_ordem(r->dir);
	return;
}

void imprime_pos_ordem(ab_t *r){
	if(r == NULL) return;
	imprime_pos_ordem(r->esq);
	imprime_pos_ordem(r->dir);
	printf("%d ", r->k);
	return;
}

void imprime_em_ordem(ab_t *r){
	if(r == NULL) return;
	imprime_em_ordem(r->esq);
	printf("%d ", r->k);
	imprime_em_ordem(r->dir);
	return;
}

ab_t *busca(ab_t *r, int k){
	if(r == NULL) return NULL;
	else if(k < r->k) return busca(r->esq, k);
	else if(k > r->k) return busca(r->dir, k);
	return r;
}

ab_t *removeNo(ab_t *r, int k){
	ab_t *aux = busca(r, k), *atual, *pai, *pai2;
	int grau = 0;
	if(!aux){
		printf("Valor nao encontrado na arvore\n"); 
		return r;
	}
	if(ehFolha(aux)) {
		pai = retornaPai(r, aux);
		if(pai) {
			if(pai->dir == aux) pai->dir = NULL;
			else pai->esq = NULL;
		} else {
			r = NULL;
		}
		free(aux);
		aux = NULL;
	} else {
		grau = grauDoNo(aux);
		// AUX EH RAIZ
		if(aux == r) {
			if(grau == GRAU1) {
				if(aux->dir) r = aux->dir;
				else r = aux->esq;
				free(aux);
				aux = NULL;
			} else if(grau == GRAU2) {
				atual = maisADireita(aux);
				pai = retornaPai(r, atual);
				if(!ehFolha(atual)) {
					if(pai->dir == atual) pai->dir = atual->esq;
					else pai->esq = atual->esq;

					if(aux->dir && aux->dir != atual) 
						if(aux->dir != NULL) atual->dir = aux->dir;
						else atual->dir = NULL;
					if(aux->esq && aux->esq != atual) 
						if(aux->esq != NULL) atual->esq = aux->esq;
						else aux->esq = NULL;

					r = atual;
					free(aux);
					aux = NULL;
				} else {
					aux->k = atual->k;
					if(pai->dir == atual) pai->dir = NULL;
					if(pai->esq == atual) pai->esq = NULL;
					free(atual);
					atual = NULL;
				}
			}
		} 
		// AUX NAO EH RAIZ
		else {
			if(grau == GRAU1) {
				pai = retornaPai(r, aux);
				if(pai->dir == aux) {
					if(aux->dir) pai->dir = aux->dir;
					else pai->dir = aux->esq;
				} else if(pai->esq == aux){
					if(aux->dir) pai->esq = aux->dir;
					else pai->esq = aux->esq;
				}
				free(aux);
				aux = NULL;
			} else if(grau == GRAU2) {
				atual = maisADireita(aux);
				pai = retornaPai(r, atual);
				pai2 = retornaPai(r, aux);
				if(!ehFolha(atual)) {
					if(pai->dir == atual) {
						pai->dir = atual->esq;
					}
					else pai->esq = atual->esq;

					if(pai2->dir == aux) pai2->dir = atual;
					else pai2->esq = atual;

					if(aux->dir != atual) atual->dir = aux->dir;
					if(aux->esq != atual) atual->esq = aux->esq;
					
					free(aux);
					aux = NULL;
				} else {
					aux->k = atual->k;
					if(pai->dir == atual) pai->dir = NULL;
					else pai->esq = NULL;

					free(atual);
					atual = NULL;
				}
			}
		}
	}

	return r;
}

int ehFolha(ab_t *r) {
	return !(r->esq || r->dir);
}

ab_t *retornaPai(ab_t *r, ab_t *no) {
	if(r == NULL || ehFolha(r)) return NULL;
	if(r->esq == no || r->dir == no) return r;
	if(no->k > r->k) {
		if(r->dir) return retornaPai(r->dir, no);
		else return NULL;
	} else {
		if(r->esq) return retornaPai(r->esq, no);
		else return NULL;
	}
}

int grauDoNo(ab_t *no) {
	if(ehFolha(no)) return GRAU0;
	if(no->dir && no->esq) return GRAU2;
	return GRAU1;
}

ab_t *maisADireita(ab_t *no) {
	no = no->esq;
	while(no->dir) {
		no = no->dir;
	}
	return no;
}

void menu(void){
	printf("Escolha qual operacao deseja executar:\n");
	printf("1 Inserir um valor inteiro na arvore\n");
	printf("2 Remover um valor inteiro da arvore\n");
	printf("3 Imprimir a arvore pre-ordem\n");
	printf("4 Imprimir a arvore pos-ordem\n");
	printf("5 Imprimir a arvore em-ordem\n");
	printf("-1 para sair\n");
}