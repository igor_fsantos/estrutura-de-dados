#include "fila.h"

void cria_fila(fila_t *f){
	f->inicio = f->fim = NULL;
	return;
}

no_f *cria_no_f(void *dado, int tam_dado){
	no_f *no = (no_f *)calloc(1, sizeof(no_f));
	if(!no) return NULL;
	no->dado = calloc(1, tam_dado);
	if(!no->dado){
		free(no);
		return NULL;
	}
	memcpy(no->dado, dado, tam_dado);
	no->tam_dado = tam_dado;
	no->prox = NULL;
	return no;
}

int fila_vazia(fila_t *f){
	return (f->inicio == NULL);
}

int insere_fila(fila_t *f, void *dado, int tam_dado){
	no_f *no = cria_no_f(dado, tam_dado);
	if(!no) return F;
	if(fila_vazia(f)){
		f->inicio = f->fim = no;
		return V;
	}
	f->fim->prox = no;
	f->fim = no;
	return V;
}

int remove_fila(fila_t *f, void *dado){
	if(fila_vazia(f)) return F;
	no_f *aux = f->inicio;
	f->inicio = f->inicio->prox;
	memcpy(dado, aux->dado, aux->tam_dado);
	free(aux->dado);
	free(aux);
	if(fila_vazia(f)) f->fim = NULL;
	return V;
}

void imprime_fila(fila_t *f){
	no_f *aux = f->inicio;
	while(aux){
		printf("%d ", *(int *)aux->dado);
		aux = aux->prox;
	}
	printf("\n");
	return;
}
