#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef _FILA_C
#define _FILA_C

struct no_f{
	void *dado;
	int tam_dado;
	struct no_f *prox;
};

typedef struct no_f no_f;

struct fila_t{
	no_f *inicio;
	no_f *fim;
};

typedef struct fila_t fila_t;

#define V 1
#define F 0

void cria_fila(fila_t *f);
no_f *cria_no_f(void *dado, int tam_dado);
int fila_vazia(fila_t *f);
int insere_fila(fila_t *f, void *dado, int tam_dado);
int remove_fila(fila_t *f, void *dado);
void imprime_fila(fila_t *f);

#endif