#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef _PILHA_C
#define _PILHA_C

struct no_p{
	void *dado;
	int tam_dado;
	struct no_p *prox;	
};

typedef struct no_p no_p;

struct pilha_t{
	no_p *topo;	
};

typedef struct pilha_t pilha_t;

#define V 1
#define F 0

void cria_pilha(pilha_t *p);
int pilha_vazia(pilha_t *p);
no_p *cria_no_p(void *dado, int tam_dado);
int empilha(pilha_t *p, void *dado, int tam_dado);
int desempilha(pilha_t *p, void *dado);
void imprime_pilha(pilha_t *p);

#endif