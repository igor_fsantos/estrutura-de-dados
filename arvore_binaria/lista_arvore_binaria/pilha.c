#include "pilha.h"

void cria_pilha(pilha_t *p){
	p->topo = NULL;
	return;
}

int pilha_vazia(pilha_t *p){
	return (p->topo == NULL);
}

no_p *cria_no_p(void *dado, int tam_dado){
	no_p *no = (no_p *)calloc(1, sizeof(no_p));
	if(!no) return NULL;
	no->dado = calloc(1, tam_dado);
	if(!no->dado){
		free(no);
		return NULL;
	}
	memcpy(no->dado, dado, tam_dado);
	no->tam_dado = tam_dado;
	no->prox = NULL;
	return no;
}

int empilha(pilha_t *p, void *dado, int tam_dado){
	no_p *no = cria_no_p(dado, tam_dado);
	if(!no) return F;
	no->prox = p->topo;
	p->topo = no;
	return V;
}

int desempilha(pilha_t *p, void *dado){
	if(pilha_vazia(p)) return F;
	no_p *aux = p->topo;
	memcpy(dado, p->topo->dado, p->topo->tam_dado);
	p->topo = p->topo->prox;
	free(aux->dado);
	free(aux);
	return V;
}

void imprime_pilha(pilha_t *p){
	if(pilha_vazia(p)) return;
	no_p *aux = p->topo;
	while(aux){
		printf("%d ", *(int *)aux->dado);
		aux = aux->prox;
	}
	printf("\n");
	return;
}