#include "funcoes_arvore.h"
#include "pilha.h"
#include "fila.h"

ab_t *cria_no(int k){
	ab_t *no = (ab_t *)calloc(1, sizeof(ab_t));
	if(!no) return NULL;
	no->k = k;
	no->esq = no->dir = NULL;
	return no;
}

//QUESTAO 1
ab_t *insere(ab_t *r, int k){
	if(!r) r = cria_no(k);
	else if(k <= r->k) r->esq = insere(r->esq, k);
	else if(k > r->k) r->dir = insere(r->dir, k);
	return r;
}
//FIM

//QUESTAO 2
ab_t *busca(ab_t *r, int k){
	if(!r) return NULL;
	else if(k < r->k) return busca(r->esq, k);
	else if(k > r->k) return busca(r->dir, k);
	return r;
}
//FIM

// QUESTAO 3
	// QUESTAO 5
int grau(ab_t *r){
	if(!r->esq && !r->dir) return GRAU0;
	if(r->esq && r->dir) return GRAU2;
	return GRAU1;
}
	//FIM
//FIM

ab_t *mais_direita(ab_t *r){
	ab_t *aux = r;
	while(aux->dir) aux = aux->dir;
	return aux;
}

ab_t *retira(ab_t *r, int k){
	if(!r) return r;
	else if(k == r->k){
		if(grau(r) == GRAU0){
			free(r);
			r = NULL;
		}
		else if(grau(r) == GRAU1){
			ab_t *aux = r;
			if(r->dir) r = r->dir;
			else r = r->esq;
			free(aux);
		}
		else{
			ab_t *mdir = mais_direita(r->esq);
			r->k = mdir->k;
			r->esq = retira(r->esq, mdir->k);
		}
	}
	else if(k < r->k) r->esq = retira(r->esq, k);
	else r->dir = retira(r->dir, k);
	return r;
}
//FIM

// QUESTAO 4
void calcula_altura(ab_t *r, int *h, int *maior){
	if(!r) return;
	*h = *h + 1;
	if(*h > *maior) *maior = *h;
	if(grau(r) == GRAU0){
		*h = *h - 1;
		return;
	}
	if(r->esq) calcula_altura(r->esq, h, maior);
	if(r->dir) calcula_altura(r->dir, h, maior);
	*h = *h - 1;
	return; 
}

int altura_arvore(ab_t *r){
	int h = -1, maior = -1;
	calcula_altura(r, &h, &maior);
	return maior;	
}
//FIM

// QUESTAO 6
int altura_no(ab_t *r, int k){
	ab_t *no = busca(r, k);
	if(!no) return -1;
	return altura_arvore(no);
}
//FIM

/*// QUESTAO 6
void calcula_altura_no2(ab_t *r, ab_t *no, int *h){
	if(r == NULL) return;
	*h = *h + 1;
	if(r == no) return;
	if(no->k <= r->k) calcula_altura_no2(r->esq, no, h);
	else calcula_altura_no2(r->dir, no, h);
	return;
}

int altura_no2(ab_t *r, ab_t *no){
	int h = 0;
	calcula_altura_no2(r, no, &h);
	return h;
}

void calcula_altura_no(ab_t *r, ab_t *no, int *h, int *h_no){
	if(r == NULL) return;
	*h = *h + 1;
	if(r == no){ 
		*h_no = *h;
		return;
	}
	if(grau(r) == GRAU0){
		*h = *h - 1;
		return;
	}
	calcula_altura_no(r->esq, no, h, h_no);
	calcula_altura_no(r->dir, no, h, h_no);
	*h = *h - 1;
	return;
}

int altura_no(ab_t *r, ab_t *no){
	int h = 0, h_no = 0;
	calcula_altura_no(r, no, &h, &h_no);
	return h_no;
}
//FIM*/

// QUESTAO 7
void calcula_nivel_no(ab_t *r, ab_t *no, int *h, int *nivel_no){
	if(r == NULL) return;
	*h = *h + 1;
	if(r == no){
		*nivel_no = *h - 1;
		return;
	}
	if(grau(r) == GRAU0){
		*h = *h - 1;
		return;
	}
	calcula_nivel_no(r->esq, no, h, nivel_no);
	calcula_nivel_no(r->dir, no, h, nivel_no);
	*h = *h - 1;
	return;
}
int nivel_no(ab_t *r, ab_t *no){
	int h = -1, nivel_no = -1;
	calcula_nivel_no(r, no, &h, &nivel_no);
	return nivel_no;
}
//FIM

// QUESTAO 8
sucessores_t *sucessores(ab_t *r, int k){
	ab_t *no = busca(r, k);
	if(!no) return NULL;
	sucessores_t *s = (sucessores_t *)calloc(1, sizeof(sucessores_t));
	if(!s) return NULL;
	s->noEsq = no->esq;
	s->noDir = no->dir;
	return s;
}
//FIM

// QUESTAO 9
ab_t *predecessor(ab_t *r, ab_t *no){
	if(r == NULL || grau(r) == GRAU0) return NULL;
	if(r->esq == no || r->dir == no) return r;
	ab_t *paiEsq, *paiDir;
	paiEsq = predecessor(r->esq, no);
	if(paiEsq != NULL) return paiEsq; 
	paiDir = predecessor(r->dir, no);
	return paiDir;
}
//FIM

// QUESTAO 10
void imprime_pre_ordem(ab_t *r){
	if(!r) return;
	printf("%d ", r->k);
	imprime_pre_ordem(r->esq);
	imprime_pre_ordem(r->dir);
	return;
}

void imprime_pos_ordem(ab_t *r){
	if(!r) return;
	imprime_pos_ordem(r->esq);
	imprime_pos_ordem(r->dir);
	printf("%d ", r->k);
	return;
}

void imprime_em_ordem(ab_t *r){
	if(!r) return;
	imprime_em_ordem(r->esq);
	printf("%d ", r->k);
	imprime_em_ordem(r->dir);
	return;
}
// FIM

// QUESTAO 11
void pre_ordem_it(ab_t *r){
	pilha_t p;
	ab_t *no = (ab_t *)calloc(1, sizeof(ab_t));
	if(!no) return;
	if(r){
		cria_pilha(&p);
		empilha(&p, r, sizeof(ab_t));
		while(!pilha_vazia(&p)){
			desempilha(&p, no);
			printf("%d ", no->k);
			if(no->dir) empilha(&p, no->dir, sizeof(ab_t));
			if(no->esq) empilha(&p, no->esq, sizeof(ab_t));
		}
	}
	free(no);
	printf("\n");
	return;
}

void pos_ordem_it(ab_t *r){
	pilha_t p, p2;
	ab_t *no = (ab_t *)calloc(1, sizeof(ab_t));
	if(!no) return;
	if(r){
		cria_pilha(&p);
		cria_pilha(&p2);
		empilha(&p, r, sizeof(ab_t));
		while(!pilha_vazia(&p)){
			desempilha(&p, no);
			empilha(&p2, no, sizeof(ab_t));
			if(no->esq) empilha(&p, no->esq, sizeof(ab_t));
			if(no->dir) empilha(&p, no->dir, sizeof(ab_t));

		}
		while(!pilha_vazia(&p2)){
			desempilha(&p2, no);
			printf("%d ", no->k);
		}
	}
	free(no);
	printf("\n");
	return;
}

void em_ordem_it(ab_t *r){
	pilha_t p;
	ab_t *no = (ab_t *)calloc(1, sizeof(ab_t));
	if(!no) return;
	if(r){
		cria_pilha(&p);
		ab_t *aux = r;
		while(V){
			if(aux){
				empilha(&p, aux, sizeof(ab_t));
				aux = aux->esq;
			}
			else if(!pilha_vazia(&p)){
				desempilha(&p, no);
				printf("%d ", no->k);
				aux = no;
				aux = aux->dir;
			}
			else break;
		}
	}
	free(no);
	printf("\n");
	return;
}
//FIM

// QUESTAO 12
void percurso_largura_it(ab_t *r){
	fila_t f;
	ab_t *no = (ab_t *)calloc(1, sizeof(ab_t));
	if(!no) return;
	if(r){
		cria_fila(&f);
		insere_fila(&f, r, sizeof(ab_t));
		while(!fila_vazia(&f)){
			remove_fila(&f, no);
			printf("%d ", no->k);
			if(no->esq) insere_fila(&f, no->esq, sizeof(ab_t));
			if(no->dir) insere_fila(&f, no->dir, sizeof(ab_t));
		}
	}
	free(no);
	printf("\n");
	return;
}
//FIM

// QUESTAO 13
int numero_folhas(ab_t *r){
	if(r == NULL) return 0;
	if(grau(r) == GRAU0) return 1;
	return (numero_folhas(r->esq) + numero_folhas(r->dir));
}
//FIM

// QUESTAO 14
int estritamente_binaria(ab_t *r) {
	if(r == NULL || grau(r) == GRAU1) return F;
	if(grau(r) == GRAU2) return (estritamente_binaria(r->esq) && estritamente_binaria(r->dir));
	return V; 
}
//FIM

// QUESTAO 15
int verifica_b_completa(ab_t *r, ab_t *no, int h_arvore){
	if(altura_no2(r, no) < h_arvore-1) {
		if(grau(no) == GRAU0) return F;
		return verifica_b_completa(r, no->esq, h_arvore) && verifica_b_completa(r, no->dir, h_arvore);
	}
	return V;
}

int binaria_completa(ab_t *r){
	return estritamente_binaria(r) && verifica_b_completa(r, r, altura_arvore(r));
}
//FIM