#include "fila.h"

int main(int argc, char const *argv[])
{
	fila_t f;
	int i, dado;
	cria_fila(&f);
	for(i = 1; i <= 10; i++){
		if(insere_fila(&f, &i, sizeof(int))) printf("Inseriu %d\n", i);
		imprime_fila(&f);
	}
	printf("\n");
	for(i = 1; i <= 10; i++){
		imprime_fila(&f);
		if(remove_fila(&f, &dado)) printf("Removeu %d\n", dado);
	}
	imprime_fila(&f);

	return 0;
}