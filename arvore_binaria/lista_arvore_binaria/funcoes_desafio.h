#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef _FUNCOES_DESAFIO_C
#define _FUNCOES_DESAFIO_C

#define V 1
#define F 0

#define GRAU0 0
#define GRAU1 1
#define GRAU2 2

struct ab_d{
	int k;
	int h;
	struct ab_d *f, *l, *n, *p, *r;
};

typedef struct ab_d ab_d;

int grau(ab_d *r);
void calcula_h_no(ab_d *r, ab_d *no, int *h, int *h_no);
int altura_no(ab_d *r, ab_d *no);
ab_d *predecessor(ab_d *r, ab_d *no);
ab_d *cria_no(int k);
ab_d *insere(ab_d *r, ab_d *pinicio, int k);
void pre_ordem(ab_d *r);
ab_d *removeNo(ab_d *raiz, int k);

#endif