#include "funcoes_arvore.h"
#include "pilha.h"
#include "fila.h"

int main(int argc, char const *argv[])
{
	ab_t *arvore = NULL, *teste = NULL, *pai = NULL;
	//arvore = insere(arvore, 60);
	arvore = insere(arvore, 50);
	arvore = insere(arvore, 40);
	arvore = insere(arvore, 39);
	arvore = insere(arvore, 42);
	arvore = insere(arvore, 41);
	arvore = insere(arvore, 45);
	arvore = insere(arvore, 44);
	arvore = insere(arvore, 46);
	arvore = insere(arvore, 60);
	//arvore = insere(arvore, 50);
	//arvore = insere(arvore, 44);
	//arvore = insere(arvore, 70);
	arvore = insere(arvore, 53);
	arvore = insere(arvore, 62);
	//arvore = insere(arvore, 71);

	imprime_pre_ordem(arvore);
	printf("\n");
	printf("Altura: %d\n", altura_arvore(arvore));
	teste = busca(arvore, 59);
	printf("\n!!!altura: %d\n", altura_no(arvore, teste));
	printf("\nNivel do no: %d\n", nivel_no(arvore, teste));
	sucessores_t *s = sucessores(arvore, 41);
	if(s){
		if(s->noEsq) printf("s->noEsq: %d\n", s->noEsq->k);
		if(s->noDir) printf("s->noDir: %d\n", s->noDir->k);
	}
	pai = predecessor(arvore, teste);
	if(pai) printf("\nPAI->k: %d\n", pai->k);	
	printf("\nPre-ordem iterativa:\n");
	pre_ordem_it(arvore);
	printf("\nPos-ordem:\n");
	imprime_pos_ordem(arvore);
	printf("\n");
	pos_ordem_it(arvore);
	printf("\nEm-ordem:\n");
	imprime_em_ordem(arvore);
	printf("\n");
	em_ordem_it(arvore);
	printf("Percurso por largura:\n");
	percurso_largura_it(arvore);

	printf("folhas: %d\n", numero_folhas(arvore));
	printf("E' estritamente binaria? %d\n", estritamente_binaria(arvore));
	printf("E' binaria completa? %d\n", binaria_completa(arvore));

	return 0;
}