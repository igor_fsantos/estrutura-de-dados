#include "pilha.h"

#ifndef _FUNCOES_ARVORE_C
#define _FUNCOES_ARVORE_C

struct ab_t{
	int k;
	struct ab_t *esq, *dir;
};

typedef struct ab_t ab_t;

struct sucessores_t{
	ab_t *noEsq, *noDir;
};

typedef struct sucessores_t sucessores_t;

#define V 1
#define F 0

#define GRAU0 0
#define GRAU1 1
#define GRAU2 2

ab_t *cria_no(int k);
ab_t *insere(ab_t *r, int k);
ab_t *busca(ab_t *r, int k);
ab_t *retira(ab_t *r, int k);
void imprime_pre_ordem(ab_t *r);
void imprime_pos_ordem(ab_t *r);
void imprime_em_ordem(ab_t *r);
void calcula_altura(ab_t *r, int *h, int *maior);
int altura_arvore(ab_t *r);
void calcula_altura_no2(ab_t *r, ab_t *no, int *h);
int altura_no2(ab_t *r, ab_t *no);
void calcula_altura_no(ab_t *r, ab_t *no, int *h, int *h_no);
int altura_no(ab_t *r, ab_t *no);
void calcula_nivel_no(ab_t *r, ab_t *no, int *h, int *nivel_no);
int nivel_no(ab_t *r, ab_t *no);
sucessores_t *sucessores(ab_t *r, int k);
ab_t *predecessor(ab_t *r, ab_t *no);
void pre_ordem_it(ab_t *r);
void pos_ordem_it(ab_t *r);
void em_ordem_it(ab_t *r);
void percurso_largura_it(ab_t *r);
int numero_folhas(ab_t *r);
int estritamente_binaria(ab_t *r);
int verifica_b_completa(ab_t *r, ab_t *no, int h_arvore);
int binaria_completa(ab_t *r);

#endif