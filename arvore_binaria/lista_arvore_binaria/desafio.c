#include "funcoes_desafio.h"

int main(int argc, char const *argv[])
{
	ab_d *arvore = NULL;
	/*arvore = insere(arvore, arvore, 50);
	if(arvore->f) printf("Pai do no1: %d\n", arvore->f->k);
	printf("altura do no1: %d\n", arvore->h);
	arvore = insere(arvore, arvore, 40);
	if(arvore->l->f) printf("Pai do no2: %d\n", arvore->l->f->k);
	printf("altura do no2: %d\n", arvore->l->h);
	arvore = insere(arvore, arvore, 39);
	if(arvore->l->l->f) printf("Pai do no3: %d\n", arvore->l->l->f->k);
	printf("altura do no3: %d\n", arvore->l->l->h);
	arvore = insere(arvore, arvore, 42);
	if(arvore->l->r->f) printf("Pai do no4: %d\n", arvore->l->r->f->k);
	printf("altura do no4: %d\n", arvore->l->r->h);
	printf("next do no %d: %d\n", arvore->l->l->k, arvore->l->l->n->k);
	printf("previous do no %d: %d\n", arvore->l->r->k, arvore->l->r->p->k);*/
	arvore = insere(arvore, arvore, 60);
	arvore = insere(arvore, arvore, 50);
	arvore = insere(arvore, arvore, 40);
	arvore = insere(arvore, arvore, 45);
	arvore = insere(arvore, arvore, 30);
	arvore = insere(arvore, arvore, 35);
	arvore = insere(arvore, arvore, 34);
	pre_ordem(arvore);
	printf("\n");
	arvore = removeNo(arvore, 40);
	pre_ordem(arvore);
	printf("\n");
	printf("filho: %d\n", arvore->l->l->l->r->k);
	printf("pai: %d\n", arvore->l->l->l->r->f->k);
	printf("%d irmao antes remov: %d\n", arvore->l->l->l->k, arvore->l->l->l->n->k);
	arvore = removeNo(arvore, 45);
	if(!arvore->l->l->l->n) printf("%d nao tem mais irmao\n", arvore->l->l->l->k);
	return 0;
}