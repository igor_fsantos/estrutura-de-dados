#include "funcoes_desafio.h"

int grau(ab_d *raiz){
	if((!raiz->l) && (!raiz->r)) return GRAU0;
	if((raiz->l) && (raiz->r)) return GRAU2;
	return GRAU1;
}

void calcula_h_no(ab_d *raiz, ab_d *no, int *h, int *h_no){
	if(raiz == NULL) return;
	*h = *h + 1;
	if(raiz == no){
		*h_no = *h;
		return;
	}
	if(no->k <= raiz->k) calcula_h_no(raiz->l, no, h, h_no);
	else calcula_h_no(raiz->r, no, h, h_no);
	return;
}

int altura_no(ab_d *raiz, ab_d *no){
	int h = 0, h_no = 0;
	calcula_h_no(raiz, no, &h, &h_no);
	return h_no;
}

ab_d *predecessor(ab_d *raiz, ab_d *no){
	if((raiz == NULL) || (grau(raiz) == GRAU0)) return NULL;
	if((raiz->l == no) || (raiz->r == no)) return raiz;
	ab_d *paiEsq, *paiDir;
	paiEsq = predecessor(raiz->l, no);
	if(paiEsq) return paiEsq;
	paiDir = predecessor(raiz->r, no);
	return paiDir;
}

ab_d *cria_no(int k){
	ab_d *no = (ab_d *)calloc(1, sizeof(ab_d));
	if(!no) return NULL;
	no->k = k;
	no->f = no->p = no->n = no->l = no->r = NULL;
	return no;
}

ab_d *mais_direita(ab_d *raiz){
	ab_d *aux = raiz;
	while(aux->r) aux = aux->r;
	return aux;
}

ab_d *insere(ab_d *raiz, ab_d *i, int k){
	if(i == NULL){ 
		i = cria_no(k);
		if(!raiz){ 
			raiz = i;
			i->h = altura_no(raiz, i);
		}
	}
	else if(k <= i->k) {
		i->l = insere(raiz, i->l, k);
		if(i->l && !i->l->f){
			i->l->f = predecessor(raiz, i->l);
			if(grau(i->l->f) == GRAU2){
				i->l->n = i->l->f->r;
				i->l->f->r->p = i->l;
			}
			i->l->h = altura_no(raiz, i->l);
		}
	}
	else if(k > i->k) {
		i->r = insere(raiz, i->r, k);
		if(i->r && !i->r->f){
			i->r->f = predecessor(raiz, i->r);
			if(grau(i->r->f) == GRAU2){
				i->r->p = i->r->f->l;
				i->r->f->l->n = i->r;
			}
			i->r->h = altura_no(raiz, i->r);
		}
	}
	return i;
}

void pre_ordem(ab_d *raiz){
	if(raiz == NULL) return;
	printf("%d ", raiz->k);
	pre_ordem(raiz->l);
	pre_ordem(raiz->r);
	return;
}

ab_d *removeNo(ab_d *raiz, int k){
	if(!raiz) return raiz;
	else if(k == raiz->k){
		if(grau(raiz) == GRAU0){
			if(raiz->n) raiz->n->p = NULL;
			else if(raiz->p) raiz->p->n = NULL;
			free(raiz);
			raiz = NULL;
		}
		else if(grau(raiz) == GRAU1){
			ab_d *aux = raiz;
			if(raiz->l){
				raiz->l->f = raiz->f;
				if(raiz->n) raiz->n->p = NULL;
				else if(raiz->p) raiz->p->n = NULL;
				raiz = raiz->l;
			}
			else{
				raiz->r->f = raiz->f;
				if(raiz->n) raiz->n->p = NULL;
				else if(raiz->p) raiz->p->n = NULL; 
				raiz = raiz->r;
			}
			free(aux);
		}
		else{
			ab_d *mdir = mais_direita(raiz->l);
			if(mdir->n) mdir->n->p = NULL;
			else if(mdir->p) mdir->p->n = NULL;
			if(grau(mdir) == GRAU1){
				if(mdir->l)	mdir->l->f = mdir->f;
				else mdir->r->f = mdir->f;
			}
			raiz->k = mdir->k;
			raiz->l = removeNo(raiz->l, mdir->k);
		}
	}
	else if(k < raiz->k) raiz->l = removeNo(raiz->l, k);
	else raiz->r = removeNo(raiz->r, k);
	return raiz;
}