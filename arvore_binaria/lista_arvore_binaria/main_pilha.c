#include "pilha.h"

int main(int argc, char const *argv[])
{
	pilha_t p;
	int i, dado;
	cria_pilha(&p);
	for(i = 1; i <= 10; i++){
		if(empilha(&p, &i, sizeof(int))) printf("Empilhou %d\n", i);
		imprime_pilha(&p);
	}
	printf("\n");
	for(i = 1; i <= 10; i++){
		imprime_pilha(&p);
		if(desempilha(&p, &dado)) printf("Desempilhou %d\n", dado);
	}
	return 0;
}