#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define V 1
#define F 0

struct no_p{
	void *dado;
	int tam_dado;
	int prio;
	struct no_p *prox;
};

typedef struct no_p no_p;

struct fila_t{
	no_p *inicio;
	no_p *fim;
};

typedef struct fila_t fila_t;

#ifndef _FUNCOES_FILA_H
#define _FUNCOES_FILA_H

fila_t *cria_fila(void);
int fila_vazia(fila_t *f);
int insere_ordenado(fila_t *f, void *dado, int tam_dado, int prio);
void imprime_fila_int(fila_t *f);
void imprime_fila_float(fila_t *f);
int remove_fila(fila_t *f, void *dado);

#endif
