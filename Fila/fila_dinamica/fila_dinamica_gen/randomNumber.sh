#!/bin/bash

MAXCOUNT=10
count=1
RANGE=100

while [ "$count" -le $MAXCOUNT ]
do
	number=$RANDOM
	echo $number
	priority=$RANDOM
	let "priority %= $RANGE"
	echo $priority
	let "count += 1"
done
