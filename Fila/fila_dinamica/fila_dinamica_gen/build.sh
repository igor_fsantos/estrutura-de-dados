#!/bin/bash

#COMPILANDO
gcc -c funcoes_fila.c
gcc -o fila_dinamica_prio -Wall fila_dinamica_prio.c funcoes_fila.o

#ADICIONANDO PERMISSAO PARA O SCRIPT DE NUMEROS ALEATORIOS
chmod 777 randomNumber.sh
./randomNumber.sh > input.txt

#EXECUTANDO
./fila_dinamica_prio < input.txt
