#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "funcoes_fila.h"

fila_t *cria_fila(void){
	fila_t *f;
	f = (fila_t *)calloc(1, sizeof(fila_t));
	if(!f) return NULL;
	f->inicio = NULL;
	f->fim = NULL;
	return f;
}

int fila_vazia(fila_t *f){
	return (f->inicio == NULL);
}

int insere_ordenado(fila_t *f, void *dado, int tam_dado, int prio){
	no_p *no, *ant, *atual;
	no = (no_p *)calloc(1, sizeof(no_p));
	if(!no) return F;
	no->prox = NULL;
	no->dado = calloc(1, tam_dado);
	if(!no->dado) return F;
	no->tam_dado = tam_dado;
	no->prio = prio;
	memcpy(no->dado, dado, tam_dado);
	if(fila_vazia(f)){
		f->inicio = no;
		f->fim = no;
		return V;
	}
	if(prio > f->inicio->prio){ //Quanto maior o valor de prio, mais alta e' a prioridade na fila
		no->prox = f->inicio;
		f->inicio = no;
		return V;
	}
	if(prio <= f->fim->prio){
		f->fim->prox = no;
		f->fim = no;
		return V;
	}
	ant = atual = f->inicio;
	while((atual != NULL) && (no->prio <= atual->prio)){
		ant = atual;
		atual = atual->prox;
	}
	ant->prox = no;
	no->prox = atual;
	
	return V;		
}

void imprime_fila_int(fila_t *f){
	no_p *i;
	i = f->inicio;
	while(i != NULL){
		printf("%d ", *(int*)i->dado);
		i = i->prox;
	}
	printf("\n");
}

void imprime_fila_float(fila_t *f){
	no_p *i;
	i = f->inicio;
	while(i != NULL){
		printf("%f ", *(float*)i->dado);
		i = i->prox;
	}
	printf("\n");
}

int remove_fila(fila_t *f, void *dado){
	if(fila_vazia(f)) return F;
	no_p *aux;
	aux = f->inicio;
	memcpy(dado, f->inicio->dado, f->inicio->tam_dado);
	f->inicio = f->inicio->prox;
	free(aux->dado);
	free(aux);
	if(fila_vazia(f)) f->fim = NULL;
	return V;
}