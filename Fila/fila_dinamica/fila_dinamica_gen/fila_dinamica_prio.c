//ATENCAO:
//OS ELEMENTOS QUE TEM OS VALORES DE PRIORIDADES MAIS ALTOS SERAO INSERIDOS SEMPRE NAS PRIMEIRAS POSICOES DA FILA 

#include <stdio.h>
#include <stdlib.h>
#include "funcoes_fila.h"

int main(int argc, char **argv){
	fila_t *ff;
	ff = cria_fila();
	if(!ff) exit(0);
	int i, num_elementos, dado, prio;

	printf("\n~Teste para fila de inteiros~\n\n");
	do{
		printf("Entre com o numero de elementos da fila: ");
		scanf("%d", &num_elementos);
	}while(num_elementos <= 0);

	for(i = 1; i <= num_elementos; i++){
		printf("Entre com o valor a ser inserido: ");
		scanf("%d", &dado);
		printf("Informe a sua prioridade na fila: ");
		scanf("%d", &prio);
		if(insere_ordenado(ff, &dado, sizeof(int), prio)) printf("---Inseriu %d\n", dado);
		else printf("---Nao inseriu %d\n", dado);
		imprime_fila_int(ff);
	}

	printf("\nImpressao da fila:\n");
	imprime_fila_int(ff);

	printf("\nRemovendo os elementos da fila...\n");
	for(i = 1; i <= num_elementos; i++){
		printf("Fila: ");
		imprime_fila_int(ff);
		if(remove_fila(ff, &dado)) printf("---Removeu %d\n", dado);
		else printf("---Nao removeu\n");
	}

	free(ff);

	return 0;
}
