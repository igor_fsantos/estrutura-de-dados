#include <stdio.h>
#include <stdlib.h>

#define V 1
#define F 0

struct no_t{
	int dado;
	struct no_t *prox;
};

typedef struct no_t no_t;

struct fila_t{
	no_t *inicio;
	no_t *fim;
};

typedef struct fila_t fila_t;

struct no_p{
	void *dado;
	int tam_dado;
	int prio;
	struct no_p *prox;
};

int insere_ordenado(fila_t *f, int dado){
	no_t *no, *ant, *atual;
	no = (no_t *)calloc(1, sizeof(no_t));
	if(!no) return F;
	no->prox = NULL;
	no->dado = dado;
	if(fila_vazia(f)){
		f->inicio = f->fim = no;
		return V;		
	}
	if(dado < f->inicio->dado){
		no->prox = f->inicio;
		f->inicio = no;
		return V;	
	}
	if(dado > f->fim->dado){
		f->fim->prox = no;
		f->fim = no;
		return V;
	}
	ant = atual = f->inicio;
	while((atual != NULL) && (no->dado >= atual->dado)){
		ant = atual;
		atual = atual->prox;
	}
	ant->prox = no;
	no->prox = atual;
	return V;	
}
