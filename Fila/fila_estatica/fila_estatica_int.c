#include <stdio.h>

#define MAX_ITENS 10
#define F 0
#define V 1

struct fila_t{
	int itens[MAX_ITENS];
	int inicio;
	int fim;
	int qtd;
};
typedef struct fila_t fila_t;

void cria_fila(fila_t *f){
	f->inicio = 0;
	f->fim = 0;
	f->qtd = 0;
}

int fila_cheia(fila_t *f) {
	return (f->qtd == MAX_ITENS);
}

int insere_fila(fila_t *f, int dado){
	if (fila_cheia(f)) return F;
	f->itens[f->fim] = dado;
	f->fim = (f->fim + 1) % MAX_ITENS;
	f->qtd++;
	return V;	
}

int fila_vazia(fila_t *f){
	return (f->qtd == 0);
}

int retira_fila(fila_t *f, int *dado){
	if (fila_vazia(f)) return F;
	*dado = f->itens[f->inicio];
	f->inicio = (f->inicio + 1) % MAX_ITENS;
	f->qtd--;
	return V;
}

int main(void) {
	int i, dado;
	fila_t ff;
	cria_fila(&ff);
	for (i=0; i<13; i++){
		dado = i+10;
		if (insere_fila(&ff, dado)) printf("Inseriu %d\n", dado);
		else printf("--- Nao inseriu %d\n", dado);
	}


	if (retira_fila(&ff, &dado)) printf("Retirou %d, inicio(%d), fim(%d)\n", dado, 
		ff.inicio, ff.fim);
	if (retira_fila(&ff, &dado)) printf("Retirou %d, inicio(%d), fim(%d)\n", dado, 
		ff.inicio, ff.fim);
	if (retira_fila(&ff, &dado)) printf("Retirou %d, inicio(%d), fim(%d)\n", dado, 
		ff.inicio, ff.fim);
	if (retira_fila(&ff, &dado)) printf("Retirou %d, inicio(%d), fim(%d)\n", dado, 
		ff.inicio, ff.fim);



	for (i=0; i<13; i++){
		dado = i+10;
		if (insere_fila(&ff, dado)) printf("Inseriu %d\n", dado);
		else printf("--- Nao inseriu %d\n", dado);
	}


	while (fila_vazia(&ff) == F) {
		if (retira_fila(&ff, &dado)) printf("Retirou %d, inicio(%d), fim(%d)\n", 
			dado, ff.inicio, ff.fim);
	}
	return 0;
}
