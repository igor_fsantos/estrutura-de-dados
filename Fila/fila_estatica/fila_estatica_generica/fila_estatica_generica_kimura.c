#include <stdio.h>  // printf
#include <stdlib.h> // calloc
#include <string.h> // memcpy

#define F 0
#define V 1

struct fila_t{
	void *itens;
	int inicio;
	int fim;
	int qtd;
	int tam_item;
	int max_qtd;
};
typedef struct fila_t fila_t;

fila_t *cria_fila(int tam_item, int max_qtd){
	fila_t *f;
	f = (fila_t *)calloc(1, sizeof(fila_t));
	if (!f) return NULL;
	f->itens = calloc(max_qtd, tam_item);
	if (!f->itens){
		free(f);
		return NULL;
	}
	f->inicio = 0;
	f->fim = 0;
	f->qtd = 0;
	f->tam_item = tam_item;
	f->max_qtd = max_qtd;	
	return f;
}

int fila_cheia(fila_t *f){
	return (f->qtd == f->max_qtd);
}

int insere_fila(fila_t *f, void *dado){
	if (fila_cheia(f)) return F;
	void *pfim;
	pfim = f->itens + f->fim * f->tam_item;
	memcpy(pfim, dado, f->tam_item);
	f->fim = (f->fim + 1) % f->max_qtd;
	f->qtd++;
	return V;
	
}

int fila_vazia(fila_t *f){
	return (f->qtd == 0);
}

int retira_fila(fila_t *f, void *dado){
	if (fila_vazia(f)) return F;
	void *pinicio;
	pinicio = f->itens + f->inicio * f->tam_item;
	memcpy(dado, pinicio, f->tam_item);
	f->inicio = (f->inicio + 1) % f->max_qtd;
	f->qtd--;
	return V;
}

void imprime_fila_int(fila_t *f){
	if (fila_vazia(f)) return;
	int i=0, p;
	printf("\n");
	while (i < f->qtd){
		p = (f->inicio + i) % f->max_qtd;
		printf("[%d]<-", *(int *)(f->itens + p *f->tam_item));
		//impressão
		i++;
	}
	printf("\n");	
}

int main(void){
	int i, dado;
	fila_t *ff;
	ff = cria_fila(sizeof(int), 10);
	for(i=0; i<13; i++){
		dado = i+10;
		if (insere_fila(ff, &dado)) printf("Inseriu %d\n", dado);
		else printf("----Nao inseriu %d\n", dado);
	}

	return 0;
}
