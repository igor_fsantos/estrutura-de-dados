#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define V 1
#define F 0

struct fila_t{
	void *itens;
	int inicio;
	int fim;
	int qtd;
	int tam_item;
	int max_qtd;		
};

typedef struct fila_t fila_t;

#ifndef _FUNCOES_FILA_GEN_H
#define _FUNCOES_FILA_GEN_H

fila_t *cria_fila(int tam_item, int max_qtd);
int fila_cheia(fila_t *f);
int insere_fila(fila_t *f, void *dado);
int fila_vazia(fila_t *f);
int retira_fila(fila_t *f, void *dado);
void imprime_fila_inicio_int(fila_t *f);
void imprime_fila_fim_int(fila_t *f);
int maior_elemento_int(fila_t *f);
int menor_elemento_int(fila_t *f);
int busca_elemento_int(fila_t *f, int elemento);
int substitui_elemento_int(fila_t *f, int destino, int origem);
void mata_fila(fila_t *f);

#endif