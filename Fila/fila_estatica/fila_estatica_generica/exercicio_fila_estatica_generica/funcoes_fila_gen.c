#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "funcoes_fila_gen.h"

fila_t *cria_fila(int tam_item, int max_qtd){
	fila_t *f;
	f = (fila_t *)calloc(1, sizeof(fila_t));
	if(!f) return NULL;
	f->itens = calloc(max_qtd, tam_item);
	if(!f->itens){
		free(f);
		return NULL;
	}
	f->inicio = 0;
	f->fim = 0;
	f->qtd = 0;
	f->max_qtd = max_qtd;
	f->tam_item = tam_item;
	return f;
}

int fila_cheia(fila_t *f){
	return (f->qtd == f->max_qtd);
}

int insere_fila(fila_t *f, void *dado){
	if(fila_cheia(f)) return F;
	void *pfim;
	pfim = f->itens + f->fim * f->tam_item;
	memcpy(pfim, dado, f->tam_item);
	f->fim = (f->fim + 1) % f->max_qtd;
	f->qtd++;	
	return V;
}

int fila_vazia(fila_t *f){
	return (f->qtd == 0);
}

int retira_fila(fila_t *f, void *dado){
	if(fila_vazia(f)) return F;
	void *pinicio;
	pinicio = f->itens + f->inicio * f->tam_item;
	memcpy(dado, pinicio, f->tam_item);
	f->inicio = (f->inicio + 1) % f->max_qtd;
	f->qtd--;
	return V;
}

void imprime_fila_inicio_int(fila_t *f){
	if(fila_vazia(f)) return;
	int i = 0, p;
	while(i < f->qtd){
		p = (f->inicio + i) % f->max_qtd;
		printf("%d ", *(int *)(f->itens + p * f->tam_item));
		i++;
	}
	printf("\n");
	return;
}

void imprime_fila_fim_int(fila_t *f){
	if(fila_vazia(f)) return;
	int i = 1, p;
	while(i <= f->qtd){
		p = (f->inicio + f->qtd - i) % f->max_qtd;
		printf("%d ", *(int *)(f->itens + p * f->tam_item));
		i++;
	}
	printf("\n");
	return;
}

int maior_elemento_int(fila_t *f){
	int indice, dado, i, p;
	dado = *((int *)f->itens + f->inicio);
	indice = f->inicio;
	for(i = 1; i < f->qtd; i++){
		p = (f->inicio + i) % f->max_qtd;
		if(*((int *)f->itens + p) > dado){
			dado = *((int *)f->itens + p);
			indice = p;
		}
	}
	return indice;
}

int menor_elemento_int(fila_t *f){
	int indice, dado, i, p;
	dado = *((int *)f->itens + f->inicio);
	indice = f->inicio;
	for(i = 1; i < f->qtd; i++){
		p = (f->inicio + i) % f->max_qtd;
		if(*((int *)f->itens + p) < dado){
			dado = *((int *)f->itens + p);
			indice = p;
		}
	}
	return indice;
}

int busca_elemento_int(fila_t *f, int elemento){
	int i, p;
	for(i = 0; i < f->qtd; i++){
		p = (f->inicio + i) % f->max_qtd;
		if(*((int *)f->itens + p) == elemento) return p;
	}
	return -1;
}

int substitui_elemento_int(fila_t *f, int destino, int origem){
	int indice;
	indice = busca_elemento_int(f, destino);
	if(indice != -1){
		memcpy(f->itens + indice * f->tam_item, &origem, f->tam_item);
		return V;
	}
	else return F;

}

void mata_fila(fila_t *f){
	free(f->itens);
	free(f);
	return;
}