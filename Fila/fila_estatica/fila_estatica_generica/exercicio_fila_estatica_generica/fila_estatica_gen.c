#include <stdio.h>
#include "funcoes_fila_gen.h"

int main(void) {
	fila_t *ff, *teste;
	int i, j, dado;
	float temp;

	ff = cria_fila(sizeof(int), 10);
	teste = cria_fila(sizeof(float), 10);
	for(i = 1; i <= 10; i++){
		temp = i * 1.0;
		if(insere_fila(teste, &temp)) printf("Inseriu %f\n", temp);
		else printf("---Nao inseriu %f\n", temp);
	}
	printf("\n");
	for(i = 1; i <= 13; i++){
		if(insere_fila(ff, &i))	printf("Inseriu %d\n", i);
		else printf("---Nao inseriu %d\n", i);	
	}
	printf("\n");
	imprime_fila_inicio_int(ff);
	imprime_fila_fim_int(ff);
	for(i = 1; i <= 3; i++){
		if(retira_fila(ff, &dado)) printf("Retirou %d\n", dado);
		else printf("---Nao retirou %d\n", dado);
	}
	printf("\n");
	imprime_fila_inicio_int(ff);
	imprime_fila_fim_int(ff);
	j = 4;
	for(i = 1; i <= 3; i++){
		if(insere_fila(ff, &j)) printf("Inseriu %d\n", j);
		else printf("---Nao inseriu %d\n", j);
	}
	printf("\n");
	imprime_fila_inicio_int(ff);
	imprime_fila_fim_int(ff);
	printf("Indice maior elemento: %d\n", maior_elemento_int(ff));
	printf("Indice menor elemento: %d\n", menor_elemento_int(ff));
	dado = -111;
	for(i = 1; i <= 4; i++){
		if(substitui_elemento_int(ff, j, dado)) printf("Substituiu 4 por -111\n");
		else printf("---Nao substituiu 4 por -111\n");
	}
	printf("\n");
	imprime_fila_inicio_int(ff);
	imprime_fila_fim_int(ff);
	mata_fila(teste);
	mata_fila(ff);

	return 0;
}