#include <stdio.h>
#include "funcoes_fila.h"

int main(void) {
	fila_t ff;
	int i, dado;

	cria_fila(&ff);
	for(i = 1; i <= 10; i++){
		insere_fila(&ff, i);
		printf("Inseriu: %d\n", i);
	}
	retira_fila(&ff, &dado);
	imprime_fila_inicio_int(&ff);
	printf("Indice maior: %d, elem: %d\n", maior_elemento(&ff), ff.itens[maior_elemento(&ff)]);
	printf("Indice menor: %d, elem: %d\n", menor_elemento(&ff), ff.itens[menor_elemento(&ff)]);
	insere_fila(&ff, -10);
	printf("Indice maior: %d, elem: %d\n", maior_elemento(&ff), ff.itens[maior_elemento(&ff)]);
	printf("Indice menor: %d, elem: %d\n", menor_elemento(&ff), ff.itens[menor_elemento(&ff)]);
	printf("\n%d\n", busca_elemento(&ff, 10));
	if(substitui_elemento(&ff, 10, 1608)) imprime_fila_inicio_int(&ff);

	return 0;
}