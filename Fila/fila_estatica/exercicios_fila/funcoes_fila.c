#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "funcoes_fila.h"

void cria_fila(fila_t *f){
	f->inicio = 0;
	f->fim = 0;
	f->qtd = 0;
}

int fila_cheia(fila_t *f){
	return (f->qtd == MAX_ITENS);
}

int insere_fila(fila_t *f, int dado){
	if(fila_cheia(f)) return F;
	f->itens[f->fim] = dado;
	f->fim = (f->fim + 1) % MAX_ITENS;
	f->qtd++;
	return V;
}

int fila_vazia(fila_t *f){
	return (f->qtd == 0);
}

int retira_fila(fila_t *f, int *dado){
	if(fila_vazia(f)) return F;
	*dado = f->itens[f->inicio];
	f->inicio = (f->inicio + 1) % MAX_ITENS;
	f->qtd--;
	return V;
}

void imprime_fila_inicio_int(fila_t *f){
	if(fila_vazia(f)) return;
	int dado, i = 0;
	while(i < f->qtd){
		dado = f->itens[(f->inicio + i) % MAX_ITENS]; 
		printf("%d ", dado);
		i++;
	}
	printf("\n");
	return;
}

void imprime_fila_fim_int(fila_t *f){
	if(fila_vazia(f)) return;
	int dado, i = 1;
	while(i <= f->qtd){
		dado = f->itens[(f->inicio + f->qtd - i) % MAX_ITENS];
		printf("%d ", dado);
		i++;
	}
	printf("\n");
	return;
}

int maior_elemento(fila_t *f){
	int indice, dado, i;
	dado = f->itens[f->inicio];
	indice = f->inicio;
	for(i = 1; i < f->qtd; i++){
		if((f->itens[(f->inicio + i) % MAX_ITENS]) > dado){
			dado = f->itens[(f->inicio + i) % MAX_ITENS];
			indice = ((f->inicio + i) % MAX_ITENS);
		}
	}
	return indice;
}

int menor_elemento(fila_t *f){
	int indice, dado, i;
	dado = f->itens[f->inicio];
	indice = f->inicio;
	for(i = 1; i < f->qtd; i++){
		if((f->itens[(f->inicio + i) % MAX_ITENS]) < dado){
			dado = f->itens[(f->inicio + i) % MAX_ITENS];
			indice = ((f->inicio + i) % MAX_ITENS);
		}
	}
	return indice;
}

int busca_elemento(fila_t *f, int elemento){
	int i;
	for(i = 0; i < f->qtd; i++){
		if(f->itens[(f->inicio + i) % MAX_ITENS] == elemento) return ((f->inicio +i) % MAX_ITENS);
	}
	return -1;
}

int substitui_elemento(fila_t *f, int destino, int origem){
	int indice;
	indice = busca_elemento(f, destino);
	if(indice != -1){
		f->itens[indice] = origem;
		return V;
	}
	else return F;

}