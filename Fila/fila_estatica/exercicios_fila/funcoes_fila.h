#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_ITENS 10
#define V 1
#define F 0

struct fila_t{
	int itens[MAX_ITENS];
	int inicio;
	int fim;
	int qtd;		
};

typedef struct fila_t fila_t;

#ifndef _FUNCOES_FILA_H
#define _FUNCOES_FILA_H

void cria_fila(fila_t *f);
int fila_cheia(fila_t *f);
int insere_fila(fila_t *f, int dado);
int fila_vazia(fila_t *f);
int retira_fila(fila_t *f, int *dado);
void imprime_fila_inicio_int(fila_t *f);
void imprime_fila_fim_int(fila_t *f);
int maior_elemento(fila_t *f);
int menor_elemento(fila_t *f);
int busca_elemento(fila_t *f, int elemento);
int substitui_elemento(fila_t *f, int destino, int origem);

#endif