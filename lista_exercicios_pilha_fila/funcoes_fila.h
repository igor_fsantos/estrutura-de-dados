#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define V 1
#define F 0

struct no_f{
	int dado;
	struct no_f *ant, *prox;
};

typedef struct no_f no_f;

struct deque_t{
	no_f *inicio;
	no_f *fim;
	int qtd;
};

typedef struct deque_t deque_t;

#ifndef _FUNCOES_FILA_H
#define _FUNCOES_FILA_H

deque_t *cria_deque(void);
int deque_vazia(deque_t *d);
int insere_inicio(deque_t *d, int dado);
void imprime_deque_int(deque_t *d);
void destroi_deque(deque_t *d);
int retira_inicio(deque_t *d, int *dado);
int insere_fim(deque_t *d, int dado);
int retira_fim(deque_t *d, int *dado);

#endif