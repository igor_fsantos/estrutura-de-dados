#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "funcoes_fila.h"

fila_t *cria_fila(void){
	fila_t *f;
	f = (fila_t *)calloc(1, sizeof(fila_t));
	if(!f) return NULL;
	f->inicio = NULL;
	f->fim = NULL;
	f->qtd = 0;
	return f; 
}

int fila_vazia(fila_t *f){
	return (f->inicio == NULL);
}

int insere_ordenado(fila_t *f, int dado, int prio){
	if((prio != 0) && (prio != 1) && (prio != 2)) return F;
	no_t *no, *ant, *atual;
	no = (no_t *)calloc(1, sizeof(no_t));
	if(!no) return F;
	no->dado = dado;
	no->prio = prio;
	no->prox = NULL;
	f->qtd++;
	if(fila_vazia(f)){
		f->inicio = no;
		f->fim = no;
		return V;
	}
	if(prio < f->inicio->prio){
		no->prox = f->inicio;
		f->inicio = no;
		return V;
	}
	if(prio >= f->fim->prio){
		f->fim->prox = no;
		f->fim = no;
		return V;
	}
	ant = atual = f->inicio;
	while((atual != NULL) && (prio >= atual->dado)){
		ant = atual;
		atual = atual->prox;
	}
	ant->prox = no;
	no->prox = atual;
	return V;
}

void imprime_fila_int(fila_t *f){
	no_t *aux;
	aux = f->inicio;
	while(aux != NULL){
		printf("%d ", aux->dado);
		aux = aux->prox;
	}
	printf("\n");
	return;
}

void destroi_fila(fila_t *f){
	no_t *aux;
	while(!fila_vazia(f)){
		aux = f->inicio;
		f->inicio = f->inicio->prox;
		free(aux);
	}
	free(f);
	return;
}

int remove_fila(fila_t *f, int *dado){
	if(fila_vazia(f)) return F;
	no_t *aux;
	aux = f->inicio;
	*dado = f->inicio->dado;
	f->inicio = f->inicio->prox;
	f->qtd--;
	free(aux);
	return V;
}

fila_t *redistribuicao_fila(fila_t *f){
	if(f->qtd <= 1) return NULL;
	fila_t *FCA;
	no_t *aux, *pinicio;
	int dado, prio;
	FCA = cria_fila();
	prio = f->inicio->prio;
	remove_fila(f, &dado);
	insere_ordenado(FCA, dado, prio);
	pinicio = aux = f->inicio;
	f->inicio = f->inicio->prox;
	while(f->inicio != NULL){
		aux->prox = f->inicio->prox;
		prio = f->inicio->prio;
		remove_fila(f, &dado);
		insere_ordenado(FCA, dado, prio);
		aux = f->inicio;
		if(!f->inicio) break;
		f->inicio = f->inicio->prox;
	}
	f->inicio = pinicio;
	return FCA;
}