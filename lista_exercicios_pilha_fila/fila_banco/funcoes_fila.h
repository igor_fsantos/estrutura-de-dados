#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define V 1
#define F 0

struct no_t{
	int dado;
	int prio;
	struct no_t *prox;
};

typedef struct no_t no_t;

struct fila_t{
	no_t *inicio;
	no_t *fim;
	int qtd;
};

typedef struct fila_t fila_t;

#ifndef _FUNCOES_FILA_H
#define _FUNCOES_FILA_H

fila_t *cria_fila(void);
int fila_vazia(fila_t *f);
int insere_ordenado(fila_t *f, int dado, int prio);
void imprime_fila_int(fila_t *f);
void destroi_fila(fila_t *f);
fila_t *redistribuicao_fila(fila_t *f);
int remove_fila(fila_t *f, int *dado);

#endif