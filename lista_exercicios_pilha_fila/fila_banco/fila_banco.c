#include <stdio.h>
#include "funcoes_fila.h"

int main(int argc, char const *argv[])
{
	fila_t *fx, *FCA;
	int i;
	fx = cria_fila();
	printf("Inserindo os elementos em fx...\n");
	for(i = 1; i <= 10; i++){
		if(insere_ordenado(fx, i, 1)) printf("Inseriu %d\n", i);
		else printf("Nao inseriu %d\n", i);
		imprime_fila_int(fx);

	}
	printf("\nRedistribuindo os elementos...\n");
	FCA = redistribuicao_fila(fx);
	printf("FCA:\n");
	imprime_fila_int(FCA);
	printf("fx\n");
	imprime_fila_int(fx);

	destroi_fila(fx);
	destroi_fila(FCA);

	return 0;
}