#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "funcoes_fila.h"

deque_t *cria_deque(void){
	deque_t *d;
	d = (deque_t *)calloc(1, sizeof(deque_t));
	if(!d) return NULL;
	d->inicio = NULL;
	d->fim = NULL;
	d->qtd = 0;
	return d;
}

int deque_vazia(deque_t *d){
	return (d->inicio == NULL);
}

int insere_inicio(deque_t *d, int dado){
	no_f *no;
	no = (no_f *)calloc(1, sizeof(no_f));
	if(!no) return F;
	no->dado = dado;
	no->ant = NULL;
	no->prox = NULL;
	d->qtd++;
	if(deque_vazia(d)){
		d->inicio = no;
		d->fim = no;
		return V;
	}
	no->prox = d->inicio;
	d->inicio->ant = no;
	d->inicio = no;
	return V;
}

void imprime_deque_int(deque_t *d){
	no_f *aux;
	aux = d->inicio;
	while(aux != NULL){
		printf("%d ", aux->dado);
		aux = aux->prox;
	}
	printf("\n");
	return;
}

void destroi_deque(deque_t *d){
	no_f *aux;
	while(!deque_vazia(d)){
		aux = d->inicio;
		d->inicio = d->inicio->prox;
		free(aux);
		d->qtd--;
	}
	d->fim = NULL;
	free(d);
	return;
}

int retira_inicio(deque_t *d, int *dado){
	if(deque_vazia(d)) return F;
	no_f *aux;
	aux = d->inicio;
	*dado = d->inicio->dado;
	d->inicio = d->inicio->prox;
	if(d->qtd > 1) d->inicio->ant = NULL;
	free(aux);
	d->qtd--;
	if(deque_vazia(d)) d->fim = NULL;
	return V;
}

int insere_fim(deque_t *d, int dado){
	no_f *no;
	no = (no_f *)calloc(1, sizeof(no_f));
	if(!no) return F;
	no->dado = dado;
	no->ant = NULL;
	no->prox = NULL;
	d->qtd++;
	if(deque_vazia(d)){
		d->inicio = no;
		d->fim = no;
		return V;
	}
	no->ant = d->fim;
	d->fim->prox = no;
	d->fim = no;
	return V;
}

int retira_fim(deque_t *d, int *dado){
	if(deque_vazia(d)) return F;
	no_f *aux;
	aux = d->fim;
	*dado = d->fim->dado;
	d->fim = d->fim->ant;
	if(d->qtd > 1) d->fim->prox = NULL;
	free(aux);
	d->qtd--;
	if(d->qtd == 0) d->inicio = NULL;
	return V;
}