#include <stdio.h>
#include "funcoes_pilha.h"
#include "funcoes_fila.h"

int main(int argc, char const *argv[])
{
	pilha_t *px;
	deque_t *dx;
	int i;
	px = pilha_binaria(4);
	imprime_pilha_int(px);
	mata_pilha(px);

	dx = cria_deque();
	printf("\nInserindo elementos na deque...\n");
	for(i = 1; i <= 10; i++){
		if(insere_fim(dx, i)) printf("Inseriu %d\n", i);
		else printf("Nao inseriu %d\n", i);
		imprime_deque_int(dx);
	}
	printf("\nRemovendo os elementos da deque...\n");
	while(!deque_vazia(dx)){
		imprime_deque_int(dx);
		if(retira_fim(dx, &i)) printf("Retirou %d\n", i);
		else printf("Nao retirou\n");									
	}
	destroi_deque(dx);

	return 0;
}