#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define V 1
#define F 0

struct no_p{
	int dado;
	struct no_p *prox;
};

typedef struct no_p no_p;

struct pilha_t{									
	no_p *topo;									
	int qtd;									
};												
												
typedef struct pilha_t pilha_t;					

#ifndef _FUNCOES_PILHA_H
#define _FUNCOES_PILHA_H

pilha_t *cria_pilha(void);
int pilha_vazia(pilha_t *p);
int empilha(pilha_t *p, int dado);
int desempilha(pilha_t *p, int *dado);
void imprime_pilha_int(pilha_t *p);
void mata_pilha(pilha_t *p);
pilha_t *pilha_binaria(int numero);

#endif