#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "funcoes_pilha.h"

pilha_t *cria_pilha(void){
	pilha_t *p;
	p = (pilha_t *)calloc(1, sizeof(pilha_t));
	if(!p) return NULL;
	p->qtd = 0;
	p->topo = NULL;
	return p;
}

int pilha_vazia(pilha_t *p){
	return (p->qtd == 0);
}

int empilha(pilha_t *p, int dado){
	no_p *no;
	no = (no_p *)calloc(1, sizeof(no_p));
	if(!no) return F;
	no->dado = dado;
	no->prox = p->topo;
	p->topo = no;
	p->qtd++;
	return V;
}

int desempilha(pilha_t *p, int *dado){
	if(pilha_vazia(p)) return F;
	no_p *aux;
	aux = p->topo;
	*dado = p->topo->dado;
	p->topo = p->topo->prox;
	free(aux);
	p->qtd--;
	return V;
}

void imprime_pilha_int(pilha_t *p){
	no_p *aux;
	aux = p->topo;
	while(aux != NULL){
		printf("%d ", aux->dado);
		aux = aux->prox;
	}
	printf("\n");
	return;
}

void mata_pilha(pilha_t *p){
	no_p *aux;
	while(!pilha_vazia(p)){
		aux = p->topo;
		p->topo = p->topo->prox;
		p->qtd--;
		free(aux);
	}
	free(p);
	return;
}

pilha_t *pilha_binaria(int numero){
	pilha_t *result;
	result = cria_pilha();
	while(numero > 1){
		empilha(result, numero % 2);
		numero = numero/2;
	}
	empilha(result, numero);
	return result;
}

