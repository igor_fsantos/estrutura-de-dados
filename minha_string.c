#include <stdio.h>
#include <stdlib.h>

int tamanho(const char *str)
{
	int tam = 0;
	while(str[tam]) tam++;
	return (tam);
}

char *concatena (const char *s1, const char *s2)
{
	int i = 0, j = 0;
	char *cat = (char *)calloc(tamanho(s1) + tamanho(s2) + 1, sizeof(char));
	if(!cat)
	{
		printf("\n[erro]: Nao foi possivel alocar memoria");
		return (NULL);
	}
	while(s1[i++] != '\0')
	{
		cat[i-1] = s1[i-1];
	}
	i--;
	while(s2[j++] != '\0')
	{
		cat[i++] = s2[j-1];
	}
	cat[i] = '\0';
	return (cat);
}

char *copia(char *destino, const char *origem)
{
	int i = 0;
	while(origem[i++])
	{
		destino[i-1] = origem[i-1];
	}
	destino[i-1] = '\0';

	return (destino);
}