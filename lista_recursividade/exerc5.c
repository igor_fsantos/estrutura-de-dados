#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define V 1
#define F 0

struct no_t{
	int dado;
	struct no_t *prox;
};

typedef struct no_t no_t;

no_t *cria_no(int dado){
	no_t *no = (no_t *)calloc(1, sizeof(no_t));
	if(!no) return NULL;
	no->dado = dado;
	no->prox = NULL;
	return no;
}

no_t *insere_inicio(no_t *l, int dado, int *c){
	no_t *no = cria_no(dado);
	if(!no){
		*c = F;
		return l;
	}
	if(l == NULL) return no;
	no->prox = l;
	l = no;
	*c = V;
	return l;
}

no_t *destroi_lista(no_t *l){
	no_t *i = l;
	while(l){
		i = l;
		l = l->prox;
		free(i);
	}
	return l;
}

void imprime_lista(no_t *i){
	if(i == NULL) return;
	printf("%d ", i->dado);
	imprime_lista(i->prox);
	return;
}

int soma_LSE_rec(no_t *i){
	if(i == NULL) return 0;
	return (i->dado + soma_LSE_rec(i->prox));
}

int main(int argc, char const *argv[])
{
	no_t *lista = NULL;
	int i, c;
	for(i = -1; i <= 5; i++){
		lista = insere_inicio(lista, i, &c);
		if(c) printf("Inseriu %d\n", i);
		imprime_lista(lista);
		printf("\n");
	}
	printf("Soma dos elementos: %d\n", soma_LSE_rec(lista));
	lista = destroi_lista(lista);
	return 0;
}