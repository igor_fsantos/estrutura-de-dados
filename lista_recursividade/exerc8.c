#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define V 1
#define F 0
#define INICIO 0
#define FIM 1

struct no_t{
	int dado;
	int prio;
	struct no_t *ant, *prox;
};

typedef struct no_t no_t;

no_t *cria_no(int dado, int prio){
	no_t *no = (no_t *)calloc(1, sizeof(no_t));
	if(!no) return NULL;
	no->dado = dado;
	no->prio = prio;
	no->prox = no->ant = no;
	return no;
}

no_t *insere_ordenado(no_t *pinicio, no_t *i, int dado, int prio){
	if(i == NULL){
		i = cria_no(dado, prio);
		return i;
	}
	if(prio <= i->prio){
		if(i->prox == pinicio){
			no_t *no = cria_no(dado, prio);
			no->ant = i;
			i->prox = no;
			no->prox = pinicio;
			pinicio->ant = no;
			return pinicio;
		}
		else insere_ordenado(pinicio, i->prox, dado, prio);
	}
	else{
		no_t *no = cria_no(dado, prio);
		no->prox = i;
		i->ant->prox = no;
		no->ant = i->ant;
		i->ant = no;
		i = no;
	}
	return i;
}

void imprime_lista(no_t *pinicio, no_t *i, int direcao){
	if(pinicio == NULL) return;
	if(i->prox == pinicio){
		printf("%d ", i->dado);
		return;
	}
	if(direcao == INICIO) printf("%d ", i->dado);
	imprime_lista(pinicio, i->prox, direcao);
	if(direcao == FIM) printf("%d ", i->dado);
	return;
}

int main(int argc, char const *argv[])
{
	no_t *lista = NULL;
	lista = insere_ordenado(lista, lista, 10, 1);
	imprime_lista(lista, lista, INICIO);
	printf("\n");
	lista = insere_ordenado(lista, lista, 9, 2);
	imprime_lista(lista, lista, INICIO);
	printf("\n");
	lista = insere_ordenado(lista, lista, 8, 3);
	imprime_lista(lista, lista, INICIO);
	printf("\n");
	lista = insere_ordenado(lista, lista, -1, 3);
	imprime_lista(lista, lista, INICIO);
	printf("\n");
	lista = insere_ordenado(lista, lista, 11, 0);
	imprime_lista(lista, lista, INICIO);
	printf("\n");
	imprime_lista(lista, lista, FIM);
	printf("\n");
	return 0;
}