#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define V 1
#define F 0

struct no_t{
	int dado;
	struct no_t *prox;
};

typedef struct no_t no_t;

struct lista_t{
	no_t *inicio, *fim;
};

typedef struct lista_t lista_t;

no_t *cria_no(int dado){
	no_t *no = (no_t *)calloc(1, sizeof(no_t));
	if(!no) return NULL;
	no->dado = dado;
	no->prox = no;
	return no;
}

lista_t *cria_lista(void){
	lista_t *l = (lista_t *)calloc(1, sizeof(lista_t));
	if(!l) return NULL;
	l->inicio = NULL;
	l->fim = NULL;
	return l;
}

int lista_vazia(lista_t *l){
	return (l->inicio == NULL);
}

int insere_fim(lista_t *l, int dado){
	no_t *no = cria_no(dado);
	if(!no) return F;
	if(lista_vazia(l)){
		l->inicio = l->fim = no;
		return V;
	}
	no->prox = l->inicio;
	l->fim->prox = no;
	l->fim = no;
	return V;
}

void imprime_lista(lista_t *l){
	if(lista_vazia(l))	return;
	no_t *aux = l->inicio;
	while(aux->prox != l->inicio){
		printf("%d ", aux->dado);
		aux = aux->prox;
	}
	printf("%d ", aux->dado);
	printf("\n");
	return;
}

lista_t *destroi_lista(lista_t *l){
	no_t *aux = l->inicio;
	while(aux != l->fim){
		l->inicio = l->inicio->prox;
		free(aux);
		aux = l->inicio;
	}
	if(l->fim){
		l->fim->prox = NULL;
		free(l->fim);
	}
	free(l);
	return NULL;
}

no_t *busca_rec(lista_t *l, no_t *i, int dado){
	if(i == l->fim){
		if(i->dado == dado) return i;
		return NULL; 
	}
	if(i->dado == dado) return i;
	return busca_rec(l, i->prox, dado);
}

int main(int argc, char const *argv[])
{
	int i;
	lista_t *lista = cria_lista();
	no_t *aux;
	for(i = 1; i <= 5; i++){
		if(insere_fim(lista, i)) printf("Inseriu %d\n", i);
		imprime_lista(lista);
	}
	aux = busca_rec(lista, lista->inicio, 5);
	if(!aux) printf("O dado nao foi encontrado\n");
	else printf("Dado encontrado: %d\n", aux->dado);
	lista = destroi_lista(lista);
	aux = NULL;
	return 0;
}