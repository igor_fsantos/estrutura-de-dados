#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define V 1
#define F 0

struct no_t{
	int dado;
	struct no_t *prox;
};

typedef struct no_t no_t;

no_t *cria_no(int dado){
	no_t *no = (no_t *)calloc(1, sizeof(no_t));
	if(!no) return NULL;
	no->dado = dado;
	no->prox = NULL;
	return no;
}

no_t *insere_ordenado(no_t *i, int dado){
	if(i == NULL){
		i = cria_no(dado);
		return i;
	}
	if(dado >= i->dado){
		i->prox = insere_ordenado(i->prox, dado);
	}
	else{
		no_t *no = cria_no(dado);
		no->prox = i;
		i = no;
	}
	return i;
}

void imprime_lista(no_t *l){
	no_t *i = l;
	while(i){
		printf("%d ", i->dado);
		i = i->prox;
	}
	printf("\n");
	return;
}

int main(int argc, char const *argv[])
{
	no_t *lista = NULL;
	lista = insere_ordenado(lista, 10);
	imprime_lista(lista);
	lista = insere_ordenado(lista, 9);
	imprime_lista(lista);
	lista = insere_ordenado(lista, 11);
	imprime_lista(lista);
	lista = insere_ordenado(lista, 7);
	imprime_lista(lista);
	lista = insere_ordenado(lista, 80);
	imprime_lista(lista);
	return 0;
}