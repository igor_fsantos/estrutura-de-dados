#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int MDC_rec(int x, int y){
	if(y == 0) return x;
	return MDC_rec(y, x%y);
}

int main(int argc, char const *argv[])
{
	if(argc != 3){
		printf("[uso]: %s valor1 valor2\n", argv[0]);
		return 0;
	}
	printf("Resultado: %d\n", MDC_rec(atoi(argv[1]), atoi(argv[2])));
	return 0;
}