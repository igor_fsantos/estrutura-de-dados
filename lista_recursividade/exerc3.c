#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int busca_rec(int *vet, int n, int valor){
	if(n < 0) return -1;
	if(vet[n] == valor) return n;
	return busca_rec(vet, n-1, valor);
}

int main(int argc, char const *argv[])
{
	int vet[] = {1, 2, 54, 7, 8, 0, 28};
	printf("Indice: %d\n", busca_rec(vet, 6, 2));
	return 0;
}