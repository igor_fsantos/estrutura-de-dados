#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int menor_rec(int *vet, int n, int *indice){
	if(n < 0) return *indice;
	if(vet[n] < vet[*indice]) *indice = n;
	return menor_rec(vet, n-1, indice);
}

int main(int argc, char const *argv[])
{
	int vet[] = {0, 3, -67, 2, 1, 4}, indice = 0;
	indice = menor_rec(vet, 5, &indice);
	printf("Indice menor: %d\n", indice);
	return 0;
}