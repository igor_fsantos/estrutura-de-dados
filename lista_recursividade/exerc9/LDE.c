#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define V 1
#define F 0

struct no_t{
	int dado;
	int prio;
	struct no_t *ant, *prox;
};

typedef struct no_t no_t;

no_t *cria_no(int dado, int prio){
	no_t *no = (no_t *)calloc(1, sizeof(no_t));
	if(!no) return NULL;
	no->dado = dado;
	no->prio = prio;
	no->prox = no->ant = NULL;
	return no;
}

no_t *insere_ordenado(no_t *i, int dado, int prio){
	if(i == NULL){
		i = cria_no(dado, prio);
		return i;
	}
	if(prio <= i->prio){
		if(i->prox == NULL){
			no_t *no = cria_no(dado, prio);
			i->prox = no;
			no->ant = i;
			return i;
		}
		else insere_ordenado(i->prox, dado, prio);
	}
	else{
		no_t *no = cria_no(dado, prio);
		no->prox = i;
		if(i->ant) i->ant->prox = no;
		no->ant = i->ant;
		i->ant = no;
		i = no;		
	}
	return i;
}

no_t *remove_busca_rec(no_t *l, no_t *i, int k){
	if(l->dado == k){
		l = l->prox;
		if(l) l->ant = NULL;
		free(i);
		return l;
	}
	if(i->prox == NULL) return l;
	if(i->prox->dado == k){
		i = i->prox;
		i->ant->prox = i->prox;
		if(i->prox) i->prox->ant = i->ant;
		free(i);
		return l;
	}
	return remove_busca_rec(l, i->prox, k);
}

void imprime_lista_i_f(no_t *l){
	no_t *i = l;
	while(i){
		printf("%d ", i->dado);
		i = i->prox;
	}
	printf("\n");
	return;
}

void imprime_lista_f_i(no_t *l){
	no_t *i = l;
	while(i){
		printf("%d ", i->dado);
		i = i->ant;
	}
	printf("\n");
	return;
}

int main(int argc, char const *argv[])
{
	no_t *lista = NULL;
	lista = insere_ordenado(lista, 10, 1);
	imprime_lista_i_f(lista);
	lista = insere_ordenado(lista, 9, 2);
	imprime_lista_i_f(lista);
	lista = insere_ordenado(lista, 8, 3);
	imprime_lista_i_f(lista);
	lista = insere_ordenado(lista, -1, 3);
	imprime_lista_i_f(lista);
	lista = insere_ordenado(lista, 11, 0);
	imprime_lista_i_f(lista);
	lista = remove_busca_rec(lista, lista, -1);
	imprime_lista_i_f(lista);
	lista = remove_busca_rec(lista, lista, 11);
	imprime_lista_i_f(lista);
	no_t *i = lista;
	while(i->prox != NULL) i = i->prox;
	imprime_lista_f_i(i);
 	return 0;
}