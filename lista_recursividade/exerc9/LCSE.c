#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define V 1
#define F 0

struct no_t{
	int dado;
	int prio;
	struct no_t *prox;
};

typedef struct no_t no_t;

struct lista_t{
	no_t *inicio, *fim;
};

typedef struct lista_t lista_t;

lista_t *cria_lista(){
	lista_t *l = (lista_t *)calloc(1, sizeof(lista_t));
	if(!l) return NULL;
	l->inicio = l->fim = NULL;
	return l;
}

int lista_vazia(lista_t *l){
	return (l->inicio == NULL);
}

no_t *cria_no(int dado, int prio){
	no_t *no = (no_t *)calloc(1, sizeof(no_t));
	if(!no) return NULL;
	no->dado = dado;
	no->prio = prio;
	no->prox = no;
	return no;
}

int insere_ordenado(lista_t *l, no_t *i, int dado, int prio){
	if(lista_vazia(l)){
		i = cria_no(dado, prio);
		if(!i) return F;
		l->inicio = l->fim = i;
		return V;		
	}
	if(i == l->fim){
		no_t *no = cria_no(dado, prio);
		if(!no) return F;
		no->prox = l->inicio;
		l->fim->prox = no;
		l->fim = no;
		return V;
	}
	if(prio <= i->prio){
		insere_ordenado(l, i->prox, dado, prio);
	}
	else{
		no_t *no = cria_no(dado, prio);
		if(!no) return F;
		no->prox = l->inicio;
		l->inicio = no;
		l->fim->prox = l->inicio;
	}
	return V; 
}

void imprime_lista(lista_t *l){
	if(lista_vazia(l)) return;
	no_t *aux = l->inicio;
	while(aux != l->fim){
		printf("%d ", aux->dado);
		aux = aux->prox;
	}
	printf("%d ", aux->dado);
	printf("\n");
	return;
}

int remove_busca_rec(lista_t *l, no_t *i, int k){
	if(l->inicio->dado == k){
		if(l->inicio->prox == l->inicio) l->inicio = l->fim = NULL;
		else{	
			l->inicio = l->inicio->prox;
			l->fim->prox = l->inicio;
		}
		free(i);
		return V;
	}
	if(i->prox == l->inicio) return F;
	if(i->prox->dado == k){
		no_t *aux = i;
		i = i->prox;
		aux->prox = i->prox;
		if(i == l->fim) l->fim = aux;
		free(i);
		return V;
	}
	return remove_busca_rec(l, i->prox, k);
}

lista_t *destroi_lista(lista_t *l){
	no_t *aux;
	while(l->inicio != l->fim){
		aux = l->inicio;
		l->inicio = l->inicio->prox;
		free(aux);
	}
	if(l->fim){
	 	l->fim->prox = NULL;
	 	free(l->fim);
	}
	free(l);
	return NULL;
}

int main(int argc, char const *argv[])
{
	lista_t *lista = cria_lista();
	insere_ordenado(lista, lista->inicio, 10, 1);
	imprime_lista(lista);
	insere_ordenado(lista, lista->inicio, 9, 2);
	imprime_lista(lista);
	insere_ordenado(lista, lista->inicio, 8, 3);
	imprime_lista(lista);
	insere_ordenado(lista, lista->inicio, -1, 3);
	imprime_lista(lista);
	insere_ordenado(lista, lista->inicio, 11, 0);
	imprime_lista(lista);
	remove_busca_rec(lista, lista->inicio, 11);
	imprime_lista(lista);
	remove_busca_rec(lista, lista->inicio, 9);
	imprime_lista(lista);
	remove_busca_rec(lista, lista->inicio, 10);
	imprime_lista(lista);
	remove_busca_rec(lista, lista->inicio, 8);
	imprime_lista(lista);
	remove_busca_rec(lista, lista->inicio, -1);
	imprime_lista(lista);
	if(lista_vazia(lista)) printf("\nA lista esta' vazia\n");
	lista = destroi_lista(lista);
	return 0;
}
