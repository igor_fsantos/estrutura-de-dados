#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define V 1
#define F 0

struct no_t{
	int dado;
	int prio;
	struct no_t *prox;
};

typedef struct no_t no_t;

no_t *cria_no(int dado, int prio){
	no_t *no = (no_t *)calloc(1, sizeof(no_t));
	if(!no) return NULL;
	no->dado = dado;
	no->prio = prio;
	no->prox = NULL;
	return no;
}

no_t *insere_ordenado(no_t *i, int dado, int prio){
	if(i == NULL){
		i = cria_no(dado, prio);
		return i;
	}
	if(prio <= i->prio){
		i->prox = insere_ordenado(i->prox, dado, prio);
	}
	else{
		no_t *no = cria_no(dado, prio);
		no->prox = i;
		i = no;
	}
	return i;
}

no_t *remove_busca_rec(no_t *l, no_t *i, int k){
	if(l->dado == k){
		l = l->prox;
		free(i);
		return l;
	}
	if(i->prox == NULL) return l;
	if(i->prox->dado == k){
		no_t *aux = i;
		i = i->prox;
		aux->prox = i->prox;
		free(i);
		return l;
	}
	return remove_busca_rec(l, i->prox, k);
}

void imprime_lista(no_t *i){
	if(i == NULL) return;
	printf("%d ", i->dado);
	imprime_lista(i->prox);
	return;
}

no_t *destroi_lista(no_t *l){
	no_t *aux;
	while(l != NULL){
		aux = l;
		l = l->prox;
		free(aux);
	}
	return l;
}

int main(int argc, char const *argv[])
{
	no_t *lista = NULL;
	lista = insere_ordenado(lista, 10, 1);
	imprime_lista(lista);
	printf("\n");
	lista = insere_ordenado(lista, 9, 2);
	imprime_lista(lista);
	printf("\n");
	lista = insere_ordenado(lista, 8, 3);
	imprime_lista(lista);
	printf("\n");
	lista = insere_ordenado(lista, -1, 3);
	imprime_lista(lista);
	printf("\n");
	lista = insere_ordenado(lista, 11, 0);
	imprime_lista(lista);
	printf("\n");
	lista = remove_busca_rec(lista, lista, 9);
	imprime_lista(lista);
	printf("\n");
	lista = remove_busca_rec(lista, lista, 10);
	imprime_lista(lista);
	printf("\n");
	lista = destroi_lista(lista);
	return 0;
}