#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define V 1
#define F 0

struct no_t{
	int dado;
	int prio;
	struct no_t *ant, *prox;
};

typedef struct no_t no_t;

no_t *cria_no(int dado, int prio){
	no_t *no = (no_t *)calloc(1, sizeof(no_t));
	if(!no) return NULL;
	no->dado = dado;
	no->prio = prio;
	no->prox = no->ant = no;
	return no;
}

no_t *insere_ordenado(no_t *pinicio, no_t *i, int dado, int prio){
	if(i == NULL){
		i = cria_no(dado, prio);
		return i;
	}
	if(prio <= i->prio){
		if(i->prox == pinicio){
			no_t *no = cria_no(dado, prio);
			no->ant = i;
			i->prox = no;
			no->prox = pinicio;
			pinicio->ant = no;
			return pinicio;
		}
		else insere_ordenado(pinicio, i->prox, dado, prio);
	}
	else{
		no_t *no = cria_no(dado, prio);
		no->prox = i;
		i->ant->prox = no;
		no->ant = i->ant;
		i->ant = no;
		i = no;
	}
	return i;
}

no_t *remove_busca_rec(no_t *l, no_t *i, int k){
	if(l->dado == k){
		if(l->prox == l) l = NULL;
		else l = l->prox;
		if(l){
		 	l->ant = i->ant;
		 	i->ant->prox = l;
		}
		free(i);
		return l;
	}
	if(i->prox == l) return l;
	if(i->prox->dado == k){
		i = i->prox;
		i->ant->prox = i->prox;
		i->prox->ant = i->ant;
		free(i);
		return l;	
	}
	return remove_busca_rec(l, i->prox, k);
}

void imprime_lista_i_f(no_t *l){
	if(l == NULL) return;
	no_t *aux = l;
	while(aux->prox != l){
		printf("%d ", aux->dado);
		aux = aux->prox;
	}
	printf("%d ", aux->dado);
	printf("\n");
	return;
}

void imprime_lista_f_i(no_t *l){
	if(l == NULL) return;
	no_t *aux = l;
	while(aux->ant != l){
		printf("%d ", aux->dado);
		aux = aux->ant;
	}
	printf("%d ", aux->dado);
	printf("\n");
	return;
}

int main(int argc, char const *argv[])
{
	no_t *lista = NULL, *aux;
	lista = insere_ordenado(lista, lista, 10, 1);
	imprime_lista_i_f(lista);
	lista = insere_ordenado(lista, lista, 9, 2);
	imprime_lista_i_f(lista);
	lista = insere_ordenado(lista, lista, 8, 3);
	imprime_lista_i_f(lista);
	lista = insere_ordenado(lista, lista, -1, 3);
	imprime_lista_i_f(lista);
	lista = insere_ordenado(lista, lista, 11, 0);
	imprime_lista_i_f(lista);
	lista = remove_busca_rec(lista, lista, 11);
	imprime_lista_i_f(lista);
	lista = remove_busca_rec(lista, lista, 8);
	imprime_lista_i_f(lista);
	lista = remove_busca_rec(lista, lista, -1);
	imprime_lista_i_f(lista);
	aux = lista;
	while(aux->prox != lista) aux = aux->prox;
	imprime_lista_f_i(aux);
	return 0;
}