#include <stdio.h>
#include <stdlib.h>
#ifndef _MINHA_STRING_H
#define _MINHA_STRING_H

int tamanho(const char *str);
char *concatena(const char *s1, const char *s2);
char *copia(char *destino, const char *origem);

#endif