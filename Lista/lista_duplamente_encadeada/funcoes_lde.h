#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define V 1
#define F 0

struct no_t{
	int dado;
	struct no_t *ant, *prox;
};

typedef struct no_t no_t;

struct listaDE_t{
	no_t *inicio;
	no_t *fim;
};

typedef struct listaDE_t listaDE_t;

#ifndef _FUNCOES_LDE_H
#define _FUNCOES_LDE_H

listaDE_t *cria_lista(void);
int lista_vazia(listaDE_t *l);
no_t *cria_no(int dado);
int insere_inicio(listaDE_t *l, int dado);
int insere_fim(listaDE_t *l, int dado);
int remove_busca(listaDE_t *l, int dado);
int insere_ordenado(listaDE_t *l, int dado);
no_t *busca(listaDE_t *l, int dado);
int desloc_direcao_inicio(listaDE_t *l, no_t *no, int valor_desloc);
int desloc_direcao_fim(listaDE_t *l, no_t *no, int valor_desloc);
int deslocamento(listaDE_t *l, int dado, int valor_desloc, const char direcao);
listaDE_t *intersecao(listaDE_t *l1, listaDE_t *l2);
no_t *mediana(listaDE_t *l);
int remove_inicio(listaDE_t *l, int *dado);
int remove_fim(listaDE_t *l, int *dado);
void imprime_lista_inicio_fim(listaDE_t *l);
void imprime_lista_fim_inicio(listaDE_t *l);
void destroi_lista(listaDE_t *l);

#endif
