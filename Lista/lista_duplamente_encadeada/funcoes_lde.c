#include "funcoes_lde.h"

// CRIACAO DA LISTA ################################################################################
//#################################################################################################
listaDE_t *cria_lista(void){
	listaDE_t *l;
	l = (listaDE_t *)calloc(1, sizeof(listaDE_t));
	if(!l) return NULL;
	l->inicio = NULL;
	l->fim = NULL;
	return l;
}

void cria_lista2(listaDE_t *l){
	l->inicio = NULL;
	l->fim = NULL;
	return;
}
//#################################################################################################
//------------------------------------------------------------------------------------------------\\

// VERIFICA SE A LISTA ESTA' VAZIA #################################################################
//#################################################################################################
int lista_vazia(listaDE_t *l){
	return (l->inicio == NULL);
}
//#################################################################################################
//------------------------------------------------------------------------------------------------\\

// CRIACAO DE NO' ##################################################################################
//#################################################################################################
no_t *cria_no(int dado){
	no_t *no;
	no = (no_t *)calloc(1, sizeof(no_t));
	if(!no) return NULL;
	no->dado = dado;
	no->prox = NULL;
	no->ant = NULL;
	return no;
}
//#################################################################################################
//------------------------------------------------------------------------------------------------\\

// FUNCOES DE INSERCAO #############################################################################
//#################################################################################################
int insere_inicio(listaDE_t *l, int dado){
	no_t *no = cria_no(dado);
	if(!no) return F;
	if(lista_vazia(l)){
		l->inicio = no;
		l->fim = no;
		return V;	
	}
	no->prox = l->inicio;
	l->inicio->ant = no;
	l->inicio = no;
	return V;	
}

int insere_fim(listaDE_t *l, int dado){
	no_t *no = cria_no(dado);
	if(!no) return F;
	if(lista_vazia(l)){
		l->inicio = no;
		l->fim = no;
		return V;
	}
	l->fim->prox = no;
	no->ant = l->fim;
	l->fim = no;
	return V;			
}

int insere_ordenado(listaDE_t *l, int dado){
	no_t *no, *aux;
	if((lista_vazia(l)) || (dado < l->inicio->dado)){ 
		if(insere_inicio(l, dado)) return V;
		else return F;
	}
	else if(dado >= l->fim->dado){ 
		if(insere_fim(l, dado)) return V;
		else return F;
	}
	no = cria_no(dado);
	if(!no) return F;
	aux = l->inicio;
	while((aux) && (dado >= aux->dado)) aux = aux->prox;
	aux->ant->prox = no;
	no->ant = aux->ant;
	aux->ant = no;
	no->prox = aux;
	return V;
}																								
//#################################################################################################
//------------------------------------------------------------------------------------------------\\

// FUNCAO DE BUSCA - QUESTAO 1 #####################################################################
//#################################################################################################
no_t *busca(listaDE_t *l, int dado){
	no_t *i = l->inicio;
	while(i){
		if(i->dado == dado) return i;
		i = i->prox;
	}
	return NULL;
}
//#################################################################################################
//------------------------------------------------------------------------------------------------\\

// DESLOCAMENTO DE NO'- QUESTAO 2 ##################################################################
//#################################################################################################
int desloc_direcao_inicio(listaDE_t *l, no_t *no, int valor_desloc){
	if(no == l->inicio) return F;
	no_t *aux = no;
	while(aux != l->inicio){
		aux = aux->ant;
		valor_desloc--;
		if(valor_desloc == 0) break;
	}
	if(valor_desloc > 0) return F;
	if(no == l->fim) no->ant->prox = no->prox;
	else{
		no->ant->prox = no->prox;
		no->prox->ant = no->ant;	
	}
	no->prox = NULL;
	no->ant = NULL;
	if(aux == l->inicio){
		aux->ant = no;
		no->prox = aux;
		l->inicio = no;
		return V;			
	}
	aux->ant->prox = no;
	no->ant = aux->ant;
	aux->ant = no;
	no->prox = aux;
	return V;
}

int desloc_direcao_fim(listaDE_t *l, no_t *no, int valor_desloc){
	if(no == l->fim) return F;
	no_t *aux = no;
	while(aux != l->fim){
		aux = aux->prox;
		valor_desloc--;
		if(valor_desloc == 0) break;
	}
	if(valor_desloc > 0) return F;
	if(no == l->inicio) no->prox->ant = no->ant;
	else{
		no->ant->prox = no->prox;
		no->prox->ant = no->ant;	
	}
	no->prox = NULL;
	no->ant = NULL;
	if(aux == l->fim){
		aux->prox = no;
		no->ant = aux;
		return V;			
	}
	aux->prox->ant = no;
	no->prox = aux->prox;
	aux->prox = no;
	no->ant = aux;
	return V;
}

int deslocamento(listaDE_t *l, int dado, int valor_desloc, const char direcao){
	no_t *no = busca(l, dado);
	if((!no) || (valor_desloc <= 0) || ((direcao != 'i') && (direcao != 'f'))) return F;
	if(direcao == 'i'){
		if(desloc_direcao_inicio(l, no, valor_desloc)) return V;
		else return F;
	}
	else if(direcao == 'f'){
		if(desloc_direcao_fim(l, no, valor_desloc)) return V;
		else return F;
	}
}
//#################################################################################################
//------------------------------------------------------------------------------------------------\\

// INTERSECAO DE DUAS LISTAS - QUESTAO 3 ###########################################################
//#################################################################################################
listaDE_t *intersecao(listaDE_t *l1, listaDE_t *l2){
	listaDE_t *result;
	result = cria_lista();
	if(!result) return NULL;
	no_t *i = l1->inicio, *aux;
	while(i){
		aux = busca(l2, i->dado);
		if(aux) insere_fim(result, i->dado);
		i = i->prox;
	}
	return result;
}
//#################################################################################################
//------------------------------------------------------------------------------------------------\\

// CALCULA A MEDIANA DE UMA LISTA - QUESTAO 4 ##################################################### 
//#################################################################################################
no_t *mediana(listaDE_t *l){
	listaDE_t aux;
	no_t *no = l->inicio, *mediana = NULL;
	int n = 0, pos_mediana;
	cria_lista2(&aux);
	while(no){
		insere_ordenado(&aux, no->dado);
		n++;
		no = no->prox;
	}
	if(n % 2 != 0)	pos_mediana = (n+1)/2;
	else pos_mediana = n/2;
	while(pos_mediana > 1){
		aux.inicio = aux.inicio->prox;
		pos_mediana--;
	}
	mediana = busca(l, aux.inicio->dado);
	return mediana;
}
//#################################################################################################
//------------------------------------------------------------------------------------------------\\

// FUNCOES DE REMOCAO ##############################################################################
//#################################################################################################
int remove_inicio(listaDE_t *l, int *dado){
	if(lista_vazia(l)) return F;
	no_t *aux = l->inicio;
	l->inicio = l->inicio->prox;
	if(!l->inicio) l->fim = NULL;
	else l->inicio->ant = NULL;
	*dado = aux->dado;
	free(aux);
	return V;
}

int remove_fim(listaDE_t *l, int *dado){
	if(lista_vazia(l)) return F;
	no_t *aux = l->fim;
	l->fim = l->fim->ant;
	if(!l->fim) l->inicio = NULL;
	else l->fim->prox = NULL;
	*dado = aux->dado;
	free(aux);
	return V;
}

int remove_busca(listaDE_t *l, int dado){
	no_t *aux = busca(l, dado);
	int dado_aux;
	if(!aux) return F;
	if(aux == l->inicio){
		if(remove_inicio(l, &dado_aux)) return V;
		return F;
	}
	else if(aux == l->fim){
		if(remove_fim(l, &dado_aux)) return V;
		return F;
	}
	aux->ant->prox = aux->prox;
	aux->prox->ant = aux->ant;
	free(aux);
	return V;
}
//#################################################################################################
//------------------------------------------------------------------------------------------------\\

// FUNCOES DE IMPRESSAO
//#################################################################################################
void imprime_lista_inicio_fim(listaDE_t *l){
	no_t *i = l->inicio;	
	while(i){
		printf("%d ", i->dado);
		i = i->prox;
	}
	printf("\n");
	return;
}

void imprime_lista_fim_inicio(listaDE_t *l){
	no_t *i = l->fim;
	while(i){
		printf("%d ", i->dado);
		i = i->ant;	
	}
	printf("\n");
	return;
}
//#################################################################################################
//------------------------------------------------------------------------------------------------\\

// LIBERA A MEMORIA ALOCADA PARA A LISTA E PARA OS NO'S ############################################
//#################################################################################################
void destroi_lista(listaDE_t *l){
	no_t *aux;
	while(l->inicio){
		aux = l->inicio;
		l->inicio = l->inicio->prox;
		free(aux);
	}
	free(l);
	return;
}
//#################################################################################################
//------------------------------------------------------------------------------------------------\\