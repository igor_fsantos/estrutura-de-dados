#include "funcoes_lde.h"

int main(){
	listaDE_t *l1, *l2, *result;
	int i;
	no_t *busca_dado, *med_l1, *med_l2;

	l1 = cria_lista();
	l2 = cria_lista();
	printf("\nInserindo dados na lista l1:\n");
	for(i = 1; i <= 10; i++){
		if(insere_inicio(l1, i)) printf("Inseriu %d\n", i);
		else printf("Nao inseriu %d\n", i);
		imprime_lista_inicio_fim(l1);
	}
	printf("\n~Questao 1~\n");
	printf("(Ex: )Busca do no' que contem o valor 7 armazenado na lista l1:\n");
	busca_dado = busca(l1, 7);
	if(busca_dado){
		printf("Resultado:\n");
		printf("%d -[%d]- %d\n", busca_dado->ant->dado, busca_dado->dado, busca_dado->prox->dado);
	}
	else printf("O valor nao foi encontrado\n");

	printf("\nPresione enter para continuar...");
	while(getchar() != '\n');

	printf("\nInserindo dados na lista l2:\n");
	for(i = 5; i >= 1; i--){
		if(insere_fim(l2, i)) printf("Inseriu %d\n", i);
		else printf("Nao inseriu %d\n", i);
		imprime_lista_inicio_fim(l2);
	}
	printf("\n~Questao 2~\n");
	printf("l2: ");
	imprime_lista_inicio_fim(l2);
	printf("Deslocamento do no' que contem valor 4 da lista l2 duas casas em direc. ao fim\n");
	if(!deslocamento(l2, 4, 2, 'f')) printf("Nao foi possivel deslocar o no'\n");
	imprime_lista_inicio_fim(l2);
	printf("Deslocamento do no' que contem valor 2 da lista l2 duas casas em direc. ao inicio\n");
	if(!deslocamento(l2, 2, 2, 'i')) printf("Nao foi possivel deslocar o no'\n");
	imprime_lista_inicio_fim(l2);

	printf("\nPresione enter para continuar...");
	while(getchar() != '\n');

	printf("\n~Questao 3~\n");
	printf("Fazendo a intersecao dos dados da lista l1 e l2:\n");
	printf("l1: ");
	imprime_lista_inicio_fim(l1);
	printf("l2: ");
	imprime_lista_inicio_fim(l2);
	result = intersecao(l1, l2);
	printf("Resultado: ");
	imprime_lista_inicio_fim(result);

	printf("\nPresione enter para continuar...");
	while(getchar() != '\n');

	printf("\nQuestao 4~\n");
	printf("Calculando a mediana dos dados de l1\n");
	imprime_lista_inicio_fim(l1);
	med_l1 = mediana(l1);
	printf("Resultado: %d\n", med_l1->dado);
	printf("Calculando a mediana dos dados de l2\n");
	imprime_lista_inicio_fim(l2);
	med_l2 = mediana(l2);
	printf("Resultado: %d\n", med_l2->dado);

	destroi_lista(l1);
	destroi_lista(l2);
	destroi_lista(result);

	return 0;
}
