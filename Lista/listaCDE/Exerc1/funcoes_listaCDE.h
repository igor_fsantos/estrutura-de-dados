#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define V 1
#define F 0

struct no_t{
	void *dado;
	int tam_dado;
	struct no_t *ant, *prox;
};

typedef struct no_t no_t;

#ifndef _FUNCOES_LISTACDE_C
#define _FUNCOES_LISTACDE_C

no_t *cria_no(void *dado, int tam_dado);
int lista_vazia(no_t *l);
no_t *insere_inicio(no_t *l, void *dado, int tam_dado, int *c);
no_t *insere_fim(no_t *l, void *dado, int tam_dado, int *c);
no_t *remove_inicio(no_t *l, void *dado, int *c);
no_t *remove_fim(no_t *l, void *dado, int *c);
void imprime_i_f_int(no_t *l);
void imprime_f_i_int(no_t *l);

#endif