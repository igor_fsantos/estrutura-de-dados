#include "funcoes_listaCDE.h"

int main(int argc, char const *argv[])
{
	no_t *lista = NULL;
	int i, c, j;
	printf("----Inserindo elementos na lista----\n");
	for(i = 1; i <= 5; i++){
		lista = insere_fim(lista, &i, sizeof(int), &c);
		if(c) printf("Inseriu %d no fim\n", i);
		else printf("Nao inseriu %d no fim\n", i);
		j = i - 1;
		lista = insere_inicio(lista, &j, sizeof(int), &c);
		if(c) printf("Inseriu %d no inicio\n", j);
		else printf("Nao inseriu %d no inicio\n", j);
		printf("\n~Impressao~:\n");
		printf("--->Do inicio ate' o fim: ");
		imprime_i_f_int(lista);
		printf("--->Do fim ate' o inicio: ");
		imprime_f_i_int(lista);
	}
	printf("\nPRESSIONE ENTER PARA CONTINUAR...");
	while(getchar() != '\n');
	printf("\n----Removendo os elementos da lista----\n");
	for(i = 1; i <= 5; i++){
		printf("\n~Impressao~:\n");
		printf("--->Do inicio ate' o fim: ");
		imprime_i_f_int(lista);
		printf("--->Do fim ate' o inicio: ");
		imprime_f_i_int(lista);
		lista = remove_fim(lista, &j, &c);
		if(c) printf("Removeu %d do fim\n", j);
		else printf("Nao removeu elemento do fim\n");
		lista = remove_inicio(lista, &j, &c);
		if(c) printf("Removeu %d do inicio\n", j);
		else printf("Nao removeu elemento do inicio\n");
	}
	if(lista_vazia(lista)) printf("\nA lista esta' vazia\n");

	return 0;
}