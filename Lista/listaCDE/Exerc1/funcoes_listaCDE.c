#include "funcoes_listaCDE.h"

no_t *cria_no(void *dado, int tam_dado){
	no_t *no = (no_t *)calloc(1, sizeof(no_t));
	if(!no) return NULL;
	no->dado = calloc(1, tam_dado);
	if(!no->dado){
		free(no);
		return NULL;
	}
	memcpy(no->dado, dado, tam_dado);
	no->tam_dado = tam_dado;
	no->ant = NULL;
	no->prox = NULL;
	return no;
}

int lista_vazia(no_t *l){
	return (l == NULL);
}

no_t *insere_inicio(no_t *l, void *dado, int tam_dado, int *c){
	no_t *no = cria_no(dado, tam_dado);
	if(!no){
		*c = F;
		return l;
	}
	if(lista_vazia(l)){
		no->ant = no;
		no->prox = no;
		*c = V;
		return no;
	}
	no->prox = l;
	no->ant = l->ant;
	l->ant->prox = no;
	l->ant = no;
	l = no;
	*c = V;
	return l;
}

no_t *insere_fim(no_t *l, void *dado, int tam_dado, int *c){
	no_t *no = cria_no(dado, tam_dado);
	if(!no){
		*c = F;
		return l;
	}
	if(lista_vazia(l)){
		no->ant = no;
		no->prox = no;
		*c = V;
		return no;
	}
	l->ant->prox = no;
	no->ant = l->ant;
	no->prox = l;
	l->ant = no;
	*c = V;
	return l;
}

no_t *remove_inicio(no_t *l, void *dado, int *c){
	if(lista_vazia(l)){
		*c = F;
		return l;
	} 
	no_t *aux = l;
	memcpy(dado, aux->dado, aux->tam_dado);
	if(l->prox == l) l = NULL;
	else{
		l = l->prox;
		l->ant = aux->ant;
		aux->ant->prox = l;
	}
	aux->ant = NULL;
	aux->prox = NULL;
	free(aux->dado);
	free(aux);
	*c = V;
	return l;
}

no_t *remove_fim(no_t *l, void *dado, int *c){
	if(lista_vazia(l)){
		*c = F;
		return l;
	} 
	no_t *aux = l->ant;
	memcpy(dado, aux->dado, aux->tam_dado);
	if(l->prox == l) l = NULL;
	else{
		l->ant = aux->ant;
		l->ant->prox = aux->prox;
	}
	aux->ant = NULL;
	aux->prox = NULL;
	free(aux->dado);
	free(aux);
	*c = V;
	return l;
}

void imprime_i_f_int(no_t *l){
	if(lista_vazia(l)) return;
	no_t *aux = l;
	do{
		printf("%d ", *(int *)aux->dado);
		aux = aux->prox;
	}while(aux->prox != l->prox);
	printf("\n");
	return;
}

void imprime_f_i_int(no_t *l){
	if(lista_vazia(l)) return;
	no_t *aux = l->ant;
	do{
		printf("%d ", *(int *)aux->dado);
		aux = aux->ant;
	}while(aux != l->ant);
	printf("\n");
	return;	
}