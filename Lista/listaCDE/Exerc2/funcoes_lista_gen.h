#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define V 1
#define F 0
#define LISTA 2
#define DADO 1

struct no_t{
	void *dado;
	int tam_dado;
	int tipo;
	struct no_t *ant, *prox;
};

typedef struct no_t no_t;

#ifndef _FUNCOES_LISTA_GEN_H
#define _FUNCOES_LISTA_GEN_H

no_t *cria_no(void *dado, int tam_dado, int tipo);
no_t *cria_no_vazio_lista();
int lista_vazia(no_t *l);
no_t *insere_inicio(no_t *l, void *dado, int tam_dado, int tipo, int *c);
no_t *insere_fim(no_t *l, void *dado, int tam_dado, int tipo, int *c);
no_t *remove_inicio(no_t *l, void *dado, int *c);
no_t *remove_fim(no_t *l, void *dado, int *c);
no_t *libera_lista(no_t *l);
no_t *remove_generalizado_inicio(no_t *l, void *dado, int *c);
no_t *remove_generalizado_fim(no_t *l, void *dado, int *c);
no_t *insere_numeros(const char *string, no_t *no);
void imprime_i_f_int(no_t *l);
void imprime_f_i_int(no_t *l);
no_t *remove_dado(no_t *l);
no_t *destroi_lista(no_t *l);

#endif