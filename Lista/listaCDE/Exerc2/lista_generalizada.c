#include "funcoes_lista_gen.h"

int main(int argc, char const *argv[])
{
	no_t *lista = NULL, *lista2 = NULL, *lista3 = NULL;
	char string[] =  "78, 22, (1,2, (30), (10, 20), 4), 100";
	int i, c;

	printf("--Inserindo elementos na lista2--\n");
	for(i = 1; i <= 3; i++){
		lista2 = insere_inicio(lista2, &i, sizeof(int), DADO, &c);
		if(c) printf("...Inseriu %d no inicio da lista2\n", i);
		else printf("...Nao inseriu %d no inicio da lista2\n", i);
		printf("\n~IMPRESSAO~\n");
		printf("-->Do inicio ate' o fim: ");
		imprime_i_f_int(lista2);
		printf("\n");
		printf("-->Do fim ate' o inicio: ");
		imprime_f_i_int(lista2);
		printf("\n");
	}
	printf("\nPRESSIONE ENTER PARA CONTINUAR...");
	while(getchar() != '\n');

	printf("\n--Inserindo elementos na lista3--\n");
	for(i = 4; i <= 5; i++){
		lista3 = insere_fim(lista3, &i, sizeof(int), DADO, &c);
		if(c) printf("...Inseriu %d no fim da lista3\n", i);
		else printf("...Nao inseriu %d no fim da lista3\n", i);
		printf("\n~IMPRESSAO~\n");
		printf("-->Do inicio ate' o fim: ");
		imprime_i_f_int(lista3);
		printf("\n");
		printf("-->Do fim ate' o inicio: ");
		imprime_f_i_int(lista3);
		printf("\n");
	}
	printf("\nPRESSIONE ENTER PARA CONTINUAR...");
	while(getchar() != '\n');

	printf("\nInserindo a lista3 no inicio da lista2:\n");
	lista2 = insere_inicio(lista2, lista3, sizeof(no_t), LISTA, &c);
	printf("\n~IMPRESSAO lista2~\n");
	printf("-->Do inicio ate' o fim: ");
	imprime_i_f_int(lista2);
	printf("\n");
	
	printf("\nPRESSIONE ENTER PARA CONTINUAR...");
	while(getchar() != '\n');
	
	printf("\nRemovendo a lista3 do inicio da lista2:\n");
	lista2 = remove_generalizado_inicio(lista2, lista3, &c);
	if(c) printf("\nRemoveu\n");
	printf("Impressão:\n");
	imprime_i_f_int(lista2);
	printf("\n");

	lista2 = libera_lista(lista2);
	lista3 = NULL;

	printf("\nPRESSIONE ENTER PARA CONTINUAR...");
	while(getchar() != '\n');

	printf("\n~Insercao de lista de lista a partir de string~\n");
	printf("String a ser inserida na lista:\n");
	printf("%s\n", string);
	lista = insere_numeros(string, lista);
	printf("\n~IMPRESSAO~\n");
	imprime_i_f_int(lista);
	printf("\n");
	lista = libera_lista(lista);
	return 0;
}