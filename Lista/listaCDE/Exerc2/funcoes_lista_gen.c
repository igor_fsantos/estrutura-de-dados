#include "funcoes_lista_gen.h"

no_t *cria_no(void *dado, int tam_dado, int tipo){
	no_t *no = (no_t *)calloc(1, sizeof(no_t));
	if(!no) return NULL;
	no->dado = calloc(1, tam_dado);
	if(!no->dado){
		free(no);
		return NULL;
	}
	memcpy(no->dado, dado, tam_dado);
	no->tam_dado = tam_dado;
	no->tipo = tipo;
	no->ant = NULL;
	no->prox = NULL;
	return no;
}

no_t *cria_no_vazio_lista(){
	no_t *no = (no_t *)calloc(1, sizeof(no_t));
	if(!no) return NULL;
	no->dado = NULL;
	no->tam_dado = sizeof(void);
	no->tipo = LISTA;
	no->prox = no;
	no->ant = no;
	return no;
}

int lista_vazia(no_t *l){
	return (l == NULL);
}

no_t *insere_inicio(no_t *l, void *dado, int tam_dado, int tipo, int *c){
	no_t *no = cria_no(dado, tam_dado, tipo);
	if(!no){
		*c = F;
		return l;
	}
	if(lista_vazia(l)){
		no->ant = no;
		no->prox = no;
		*c = V;
		return no;
	}
	no->prox = l;
	no->ant = l->ant;
	l->ant->prox = no;
	l->ant = no;
	l = no;
	*c = V;
	return l;
}

no_t *insere_fim(no_t *l, void *dado, int tam_dado, int tipo, int *c){
	no_t *no = cria_no(dado, tam_dado, tipo);
	if(!no){
		*c = F;
		return l;
	}
	if(lista_vazia(l)){
		no->ant = no;
		no->prox = no;
		*c = V;
		return no;
	}
	l->ant->prox = no;
	no->ant = l->ant;
	no->prox = l;
	l->ant = no;
	*c = V;
	return l;
}

no_t *remove_inicio(no_t *l, void *dado, int *c){
	if(lista_vazia(l)){
		*c = F;
		return l;
	} 
	no_t *aux = l;
	memcpy(dado, aux->dado, aux->tam_dado);
	if(l->prox == l) l = NULL;
	else{
		l = l->prox;
		l->ant = aux->ant;
		aux->ant->prox = l;
	}
	aux->ant = NULL;
	aux->prox = NULL;
	free(aux->dado);
	free(aux);
	*c = V;
	return l;
}

no_t *remove_fim(no_t *l, void *dado, int *c){
	if(lista_vazia(l)){
		*c = F;
		return l;
	} 
	no_t *aux = l->ant;
	memcpy(dado, aux->dado, aux->tam_dado);
	if(l->prox == l) l = NULL;
	else{
		l->ant = aux->ant;
		l->ant->prox = aux->prox;
	}
	aux->ant = NULL;
	aux->prox = NULL;
	free(aux->dado);
	free(aux);
	*c = V;
	return l;
}

no_t *remove_dado(no_t *l){
	if(lista_vazia(l)){
		return l;
	} 
	no_t *aux = l;
	if(l->prox == l) {
		l = NULL;
	}
	else{
		l = l->prox;
		l->ant = aux->ant;
		l->ant->prox = aux->prox;
	}
	aux->ant = NULL;
	aux->prox = NULL;
	free(aux->dado);
	free(aux);
	return l;
}

no_t *libera_lista(no_t *l){
	no_t *aux = l;
	if(aux == NULL) return aux;
	if(aux->tipo == DADO) {
		aux = remove_dado(aux);
		aux = libera_lista(aux);
	}
	else if(aux->tipo == LISTA) {
		aux->dado = libera_lista(aux->dado);
		if(aux->prox != aux) {
			aux = aux->prox;
			free(aux->ant->dado);
			free(aux->ant);
			aux = libera_lista(aux);
		}
		else free(aux);
	}
	return aux;
}

no_t *remove_generalizado_inicio(no_t *l, void *dado, int *c){
	if(lista_vazia(l)){
		*c = F;
		return l;
	} 
	no_t *aux = l;
	memcpy(dado, aux->dado, aux->tam_dado);
	if(l->prox == l) l = NULL;
	else{
		l = l->prox;
		l->ant = aux->ant;
		aux->ant->prox = l;
	}
	aux->prox = aux;
	aux->ant = aux;
	aux = libera_lista(aux);
	*c = V;
	return l;
}

no_t *remove_generalizado_fim(no_t *l, void *dado, int *c){
	if(lista_vazia(l)){
		*c = F;
		return l;
	} 
	no_t *aux = l->ant;
	memcpy(dado, aux->dado, aux->tam_dado);
	if(l->prox == l) l = NULL;
	else{
		l->ant = aux->ant;
		l->ant->prox = aux->prox;
	}
	aux->prox = aux;
	aux->ant = aux;
	aux = libera_lista(aux);
	*c = V;
	return l;
}

no_t *insere_numeros(const char *string, no_t *no){
	no_t *aux = NULL;
	int c, numero, i = 0;
	char num_string[100];
	if(*string == '\0')	return no;
	if(isdigit(*string)){
		while(isdigit(*string)){
			num_string[i] = *string;
			string++;
			i++;
		}
		num_string[i] = '\0';
		numero = atoi(num_string);
		i = 0;
		no = insere_fim(no, &numero, sizeof(int), DADO, &c);
		string++;
		no = insere_numeros(string, no);
	} else if(*string == '('){
		string++;
		aux = insere_numeros(string, aux);
		no = insere_fim(no, aux, sizeof(no_t), LISTA, &c);
	} else if(*string == ')') return no;
	else {
		string++;
		if(*string == '\0') return no;
		no = insere_numeros(string, no);
	}
	return no;	
}

void imprime_i_f_int(no_t *l){
	if(lista_vazia(l)) return;
	no_t *aux = l;
	do{
		if(aux->tipo == DADO) {
			printf("%d ", *(int *)aux->dado);
		}
		else if(aux->tipo == LISTA){
			imprime_i_f_int(aux->dado);
		}
		aux = aux->prox;
	}while(aux->prox != l->prox);
	return;
}

void imprime_f_i_int(no_t *l){
	if(lista_vazia(l)) return;
	no_t *aux = l->ant;
	do{
		if(aux->tipo == DADO){
			printf("%d ", *(int *)aux->dado);
		}
		else if(aux->tipo == LISTA){
			imprime_f_i_int(aux->dado);
		}
		aux = aux->ant;
	}while(aux != l->ant);
	return;	
}