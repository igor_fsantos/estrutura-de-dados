#include "funcoes_lista.h"

int main(int argc, char const *argv[])
{

	no_t *lista = NULL;
	int i;
	
	printf("\nInserindo elementos...\n");
	for(i = 1; i <= 10; i++){
		lista = insere_lista(lista, &i, sizeof(int));
		printf("Inseriu: %d\n", i);
		imprime_lista_int(lista);
	}
	printf("\nRemovendo os elementos...\n");
	for(i = 10; i >= 1; i--){
		imprime_lista_int(lista);
		lista = remove_lista_int(lista, &i);
		printf("Removeu %d\n", i);
	}
	if(lista_vazia(lista)) printf("A lista esta' vazia\n");
	imprime_lista_int(lista);

	return 0;
}
