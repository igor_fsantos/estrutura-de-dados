#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct no_t{
	void *dado;
	struct no_t *prox;
};

typedef struct no_t no_t;

#ifndef _FUNCOES_LISTA_H
#define _FUNCOES_LISTA_H

int lista_vazia(no_t *l);
no_t *insere_lista(no_t *l, void *dado, int tam_dado);
void imprime_lista_int(no_t *l);
no_t *remove_lista_int(no_t *l, void *dado);
no_t *remove_lista_float(no_t *l, void *dado);
no_t *remove_lista_char(no_t *l, void *dado);

#endif