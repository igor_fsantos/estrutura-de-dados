#include "funcoes_lista.h"

int lista_vazia(no_t *l){
	return (l == NULL);
}

no_t *insere_lista(no_t *l, void *dado, int tam_dado){ //Os elementos são inseridos no inicio da lista
	no_t *no, *aux;
	no = (no_t *)calloc(1, sizeof(no_t));
	if(!no) return NULL;
	no->dado = calloc(1, sizeof(tam_dado));
	if(!no->dado){
		free(no);
		return NULL;
	}
	no->prox = NULL;
	memcpy(no->dado, dado, tam_dado);
	if(lista_vazia(l)){
		no->prox = no;
		return no;
	}
	aux = l;
	while(aux->prox != l){
		aux = aux->prox;
	}
	aux->prox = no;
	no->prox = l;
	l = no;
	return l;
}

no_t *remove_lista_int(no_t *l, void *dado){
	if(lista_vazia(l)) return l;
	no_t *ant = l, *atual = l->prox;
	while((atual->prox != l->prox) && (*(int*)dado != *(int*)atual->dado)) {
		ant = atual;
		atual = atual->prox;
	}
	if((*(int*)dado) == (*(int*)atual->dado)) {
		if(l->prox == l) {
			l = NULL;
		} else {
			if(l == atual) l = l->prox;
			ant->prox = atual->prox;	
		}
		free(atual->dado);
		free(atual);	
	}		
	return l;
}

no_t *remove_lista_float(no_t *l, void *dado){
	if(lista_vazia(l)) return l;
	no_t *ant = l, *atual = l->prox;
	while((atual->prox != l->prox) && (*(float*)dado != *(float*)atual->dado)) {
		ant = atual;
		atual = atual->prox;
	}
	if((*(float*)dado) == (*(float*)atual->dado)) {
		if(l->prox == l) {
			l = NULL;
		} else {
			if(l == atual) l = l->prox;
			ant->prox = atual->prox;	
		}
		free(atual->dado);
		free(atual);	
	}		
	return l;
}

no_t *remove_lista_char(no_t *l, void *dado){
	if(lista_vazia(l)) return l;
	no_t *ant = l, *atual = l->prox;
	while((atual->prox != l->prox) && (*(char*)dado != *(char*)atual->dado)) {
		ant = atual;
		atual = atual->prox;
	}
	if((*(char*)dado) == (*(char*)atual->dado)) {
		if(l->prox == l) {
			l = NULL;
		} else {
			if(l == atual) l = l->prox;
			ant->prox = atual->prox;	
		}
		free(atual->dado);
		free(atual);	
	}		
	return l;
}

void imprime_lista_int(no_t *l){
	if(lista_vazia(l)) return;  
	no_t *aux;
	aux = l->prox;
	printf("%d ", *(int *)l->dado);
	while(aux != l){
		printf("%d ", *(int *)aux->dado);
		aux = aux->prox;
	}
	printf("\n");
	return;
}