#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct pilha_t{
	void *itens;
	int tam_item;
	int topo;
	int tam_pilha;
};

typedef struct pilha_t pilha_t;

#define VAZIO -1
#define V 1
#define F 0

//criacao
int cria_pilha(pilha_t *p, int ti, int tp){
	p->tam_item = ti;
	p->topo = VAZIO;
	p->tam_pilha = tp;
	p->itens = calloc(tp, ti);
	if(p->itens == NULL) return F;
	return V; 
}
//empilhamento
int pilha_cheia(pilha_t *p){
	if(p->topo == p->tam_pilha-1) return V;
	return F;
}
int empilha(pilha_t *p, void *dado){
	if(pilha_cheia(p)) return F;
	p->topo++;
	void *dst;
	dst = p->itens + p->topo * p->tam_item; //itens[topo]
	memcpy(dst, dado, p->tam_item);
	return V;	
}
//desempilhamento
int pilha_vazia(pilha_t *p){
	if(p->topo == VAZIO) return V;
	return F;
}
int desempilha(pilha_t *p, void *dado){
	if(pilha_vazia(p)) return F;
	void *origem;
	origem = p->itens + p->topo * p->tam_item;
	memcpy(dado, origem, p->tam_item);
	p->topo--;
	return V;
}

void mata_pilha(pilha_t *p){
	free(p->itens);
	p->topo = VAZIO;
	p->tam_item = 0;
	p->tam_pilha = 0;
}

//imprime pilha
void imprime_pilha(pilha_t *p){
	int dado, i;
	for(i = p->topo; i >= 0; i--){
		dado = *(int *)(p->itens + i * p->tam_item);
		printf("Elemento %d\n", dado);
	}
}
int main(int argc, char **argv){
	if(argc != 2){
		printf("[uso]: %s <tam_pilha>\n", argv[0]);
		return 0;
	}
	int tam_p, di, i;
	tam_p = atoi(argv[1]);
	pilha_t pi;
	cria_pilha(&pi, sizeof(int), tam_p);
	for(i = 0; i < tam_p; i++){
		di = i*10;
		if(empilha(&pi, &di)) printf("Empilhou %d\n", di);
		else printf("---Nao empilhou %d\n", di);
	}
	while(!pilha_vazia(&pi)){
		printf("------------\n");
		imprime_pilha(&pi);
		desempilha(&pi, &di);
		printf("------------\n");
	}
	imprime_pilha(&pi);
	mata_pilha(&pi);
	return 0;
}