#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "funcoes_pilha.h"

pilha_t *cria_pilha(void){
	pilha_t *p;
	p = (pilha_t *)calloc(1, sizeof(pilha_t));
	if(!p) return NULL;
	p->topo = NULL;
	p->qtd = 0;
	return p;
}

int empilha(pilha_t *p, void *dado, int tam_dado){
	no_t *no;
	no = (no_t *)calloc(1, sizeof(no_t));
	if(!no) return F;
	no->dado = calloc(1, tam_dado);
	if(!no->dado) return F;
	no->tam_dado = tam_dado;
	memcpy(no->dado, dado, tam_dado);
	no->prox = p->topo;
	p->topo = no;
	p->qtd++;
	return V;
}

int pilha_vazia(pilha_t *p){
	return (p->topo == NULL);
}

int desempilha(pilha_t *p, void *dado){
	if(pilha_vazia(p)) return F;
	no_t *aux;
	aux = p->topo;
	memcpy(dado, aux->dado, aux->tam_dado);
	p->topo = p->topo->prox;
	free(aux->dado);
	free(aux);
	p->qtd--;
	return V;
}

void mata_pilha(pilha_t *p){
	no_t *aux;
	while(!pilha_vazia(p)){
		aux = p->topo;
		p->topo = p->topo->prox;
		free(aux->dado);
		free(aux);
	}
	free(p);
	return;	
}

void imprime_pilha_int(pilha_t *p){
	if(pilha_vazia(p)) return;
	void *topo;
	int dado, i = 0;
	topo = p->topo;
	while(i < p->qtd){
		dado = *(int *)p->topo->dado;
		printf("%d ", dado);
		p->topo = p->topo->prox;
		i++;
	}
	printf("\n");
	p->topo = topo;
	return;
}

void imprime_pilha_char(pilha_t *p){
	if(pilha_vazia(p)) return;
	no_t *ptr;
	char dado;
	int i = 0;
	ptr = p->topo;
	while(i < p->qtd){
		dado = *(char *)ptr->dado;
		printf("%c ", dado);
		ptr = ptr->prox;
		i++;
	}
	printf("\n");
	return;
}

int menor_elemento_int(pilha_t *p){
	int elemento, i = 1;
	void *topo;
	topo = p->topo;
	elemento = *(int *)p->topo->dado;
	p->topo = p->topo->prox;
	while(i < p->qtd){
		if(*(int *)p->topo->dado < elemento) elemento = *(int *)p->topo->dado;
		p->topo = p->topo->prox;
		i++;
	}
	p->topo = topo;
	return (elemento);
}

int maior_elemento_int(pilha_t *p){
	int elemento, i = 1;
	void *topo;
	topo = p->topo;
	elemento = *(int *)p->topo->dado;
	p->topo = p->topo->prox;
	while(i < p->qtd){
		if(*(int *)p->topo->dado > elemento) elemento = *(int *)p->topo->dado;
		p->topo = p->topo->prox;
		i++;
	}
	p->topo = topo;
	return (elemento);	
}

no_t *busca_elemento_int(pilha_t *p, int elemento){
	void *topo;
	no_t *aux = NULL;
	int i = 0;
	topo = p->topo;
	while(i < p->qtd){
		if(*(int *)p->topo->dado == elemento){
			aux = p->topo;
			break;
		}
		p->topo = p->topo->prox;
		i++;
	}
	p->topo = topo;
	return aux;
}

int atualiza_valor_int(pilha_t *p, int destino, int origem){
	no_t *aux;
	aux = busca_elemento_int(p, destino);
	if(!aux) return F;
	memcpy(aux->dado, &origem, p->topo->tam_dado);
	return V;
}

pilha_t *inverte_pilha(pilha_t *p){
	no_t *aux; 
	pilha_t *pilha_inv;
	int i = 0;
	pilha_inv = cria_pilha();
	aux = p->topo;
	while(i < p->qtd){
		empilha(pilha_inv, aux->dado, aux->tam_dado);
		aux = aux->prox;
		i++;
	}
	return pilha_inv;
}

/*pilha_t *empilha_duas_pilhas(pilha_t *p1, pilha_t *p2, int tam_dado){
	pilha_t *result, *aux1, *aux2;
	void *dado;
	int i;
	result = cria_pilha();
	aux1 = cria_pilha();
	aux2 = cria_pilha();
	dado = calloc(1, tam_dado);
	if(!dado) return NULL;
	aux1 = inverte_pilha(p1);
	aux2 = inverte_pilha(p2);
	while(!pilha_vazia(aux1)){
		desempilha(aux1, dado);
		empilha(result, dado, p1->topo->tam_dado);
	}
	while(!pilha_vazia(aux2)){
		desempilha(aux2, dado);
		empilha(result, dado, p2->topo->tam_dado);
	}
	free(dado);
	return result;	
}*/

pilha_t *empilha_duas_pilhas(pilha_t *p1, pilha_t *p2){
	pilha_t *result, *aux1, *aux2;
	no_t *no;
	result = cria_pilha();
	aux1 = cria_pilha();
	aux2 = cria_pilha();
	aux1 = inverte_pilha(p1);
	aux2 = inverte_pilha(p2);
	no = aux1->topo;
	while(no != NULL){
		empilha(result, no->dado, no->tam_dado);
		no = no->prox;
	}
	no = aux2->topo;
	while(no != NULL){
		empilha(result, no->dado, no->tam_dado);
		no = no->prox;
	}
	return result;
}

int verifica_expressao(const char *expressao){
	int parenteses = 0, colchetes = 0, i = 0;
	while(expressao[i]){
		if(expressao[i] == '(') parenteses++;		
		else if(expressao[i] == '[') colchetes++;
		else if(expressao[i] == ')'){
			parenteses--;
			if(parenteses < 0) return F;
		}
		else if(expressao[i] == ']'){
			colchetes--;
			if(colchetes < 0) return F;
		}
		i++;
	}
	return((parenteses + colchetes) == 0);
}

int eh_operador(const char caractere){
	return((caractere == '+') || (caractere == '-') || (caractere == '*') || (caractere == '/'));
}

void separa_expressao(pilha_t *expressao, pilha_t *p1, pilha_t *p2){
	pilha_t *aux;
	void *dado;
	char caractere;
	aux = inverte_pilha(expressao);
	aux = inverte_pilha(aux);
	dado = calloc(1, expressao->topo->tam_dado);
	if(!dado){
		printf("[erro]: Nao foi possivel alocar memoria\n");
		return;
	}
	while(aux->topo != NULL){
		desempilha(aux, dado);
		caractere = *(char *)dado;
		if(eh_operador(caractere)) empilha(p2, &caractere, aux->topo->tam_dado);
		else if(isdigit(caractere) || isalpha(caractere)){
			empilha(p1, &caractere, aux->topo->tam_dado);
		}
	}
	return;
}

int ordem_precedencia(const char caractere){
	if((isdigit(caractere) || isalpha(caractere))) return 0;
	else if((caractere == '+') || (caractere == '-')) return 1;
	else if((caractere == '*') || (caractere == '/')) return 2;
	else if(caractere == '[') return 3;
	else if(caractere == '(') return 4;

	return 0;
}

pilha_t *transforma_posfixa(const char *expressao){
	if(!verifica_expressao(expressao)) return NULL;
	pilha_t *posfixa, *aux;
	char caractere, dado;
	int i = 0;
	posfixa = cria_pilha();
	aux = cria_pilha();
	while(expressao[i]){
		caractere = expressao[i];
		if((isalpha(caractere)) || (isdigit(caractere))){
			empilha(posfixa, &caractere, sizeof(char));
		}
		else{
			if((pilha_vazia(aux)) && (caractere != ')') && (caractere != ']')){
				empilha(aux, &caractere, sizeof(char));
			} 
			else if(caractere == ')'){
				while(V){
					desempilha(aux, &dado);
					if(dado == '(') break;
					empilha(posfixa, &dado, sizeof(char));
				}
			}
			else if(caractere == ']'){
				while(V){
					desempilha(aux, &dado);
					if(dado == '[') break;
					empilha(posfixa, &dado, sizeof(char));
				}
			}
			else if(ordem_precedencia(*(char *)aux->topo->dado) >= ordem_precedencia(caractere)){
				if((*(char *)aux->topo->dado == '(') || (*(char *)aux->topo->dado == '[')){
					empilha(aux, &caractere, sizeof(char));
				}
				else{
					desempilha(aux, &dado);
					empilha(posfixa, &dado, sizeof(char));
					empilha(aux, &caractere, sizeof(char));
				}
			}
			else if(ordem_precedencia(*(char *)aux->topo->dado) < ordem_precedencia(caractere)){
				empilha(aux, &caractere, sizeof(char));
			}
		}
		i++;	
	}
	while(!pilha_vazia(aux)){
		desempilha(aux, &dado);
		empilha(posfixa, &dado, sizeof(char));
	}
	posfixa = inverte_pilha(posfixa);
	return posfixa;
}