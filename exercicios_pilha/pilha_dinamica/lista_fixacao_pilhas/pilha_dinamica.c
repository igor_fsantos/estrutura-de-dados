#include <stdio.h>
#include <ctype.h>
#include "funcoes_pilha.h"

int main(int argc, char const *argv[])
{
	/*pilha_t *posfixa;
	posfixa = cria_pilha();
	posfixa = transforma_posfixa("[(A+B)*D+E]/(F+A*D)+C");
	imprime_pilha_char(posfixa);
	mata_pilha(posfixa);*/

	pilha_t *result, *p1, *p2;
	int i;

	p1 = cria_pilha();
	p2 = cria_pilha();
	printf("Empilhando os elementos na p1...\n");
	for(i = 1; i <= 10; i++){
		if(empilha(p1, &i, sizeof(int))) printf("Empilhou %d\n", i);
		else printf("Nao empihlou %d\n", i);
		imprime_pilha_int(p1);
	}
	printf("\nEmpilhando os elementos na p2...\n");
	for(i = 11; i <= 20; i++){
		if(empilha(p2, &i, sizeof(int))) printf("Empilhou %d\n", i);
		else printf("Nao empihlou %d\n", i);
		imprime_pilha_int(p2);	
	}
	printf("\nEmpilhando p1 e p2 em result...\n");
	result = empilha_duas_pilhas(p1, p2);
	imprime_pilha_int(result);
	mata_pilha(p1);
	mata_pilha(p2);
	mata_pilha(result);

	return 0;
}