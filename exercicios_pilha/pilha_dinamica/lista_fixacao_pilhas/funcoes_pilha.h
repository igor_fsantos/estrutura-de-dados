#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define V 1
#define F 0

struct no_t{
	void *dado;
	int tam_dado;
	struct no_t *prox;
};

typedef struct no_t no_t;

struct pilha_t{
	no_t *topo;
	int qtd;	
};

typedef struct pilha_t pilha_t;

#ifndef _FUNCOES_PILHA_H
#define _FUNCOES_PILHA_H

pilha_t *cria_pilha(void);
int empilha(pilha_t *p, void *dado, int tam_dado);
int pilha_vazia(pilha_t *p);
int desempilha(pilha_t *p, void *dado);
void mata_pilha(pilha_t *p);
void imprime_pilha_int(pilha_t *p);
void imprime_pilha_char(pilha_t *p);
int menor_elemento_int(pilha_t *p);
int maior_elemento_int(pilha_t *p);
no_t *busca_elemento_int(pilha_t *p, int elemento);
int atualiza_valor_int(pilha_t *p, int destino, int origem);
pilha_t *inverte_pilha(pilha_t *p);
pilha_t *empilha_duas_pilhas(pilha_t *p1, pilha_t *p2);
int verifica_expressao(const char *expressao);
int eh_operador(const char caractere);
void separa_expressao(pilha_t *expressao, pilha_t *p1, pilha_t *p2);
int ordem_precedencia(const char caractere);
pilha_t *transforma_posfixa(const char *expressao);

#endif