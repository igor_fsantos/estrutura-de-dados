#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define V 1
#define F 0

struct no_t{
	void *dado;
	int tam_dado;
	struct no_t *prox;
};

typedef struct no_t no_t;

struct pilha_t{
	no_t *topo;
	int qtd;
};

typedef struct pilha_t pilha_t;

pilha_t *cria_pilha(void){
	pilha_t *p;
	p = (pilha_t *)calloc(1, sizeof(pilha_t));
	if(!p) return NULL;
	p->topo = NULL;
	p->qtd = 0;
	return p;
}

int empilha(pilha_t *p, void *dado, int tam_dado){
	no_t *no;
	no = (no_t *)calloc(1, sizeof(no_t));
	if(!no) return F;
	no->dado = calloc(1, tam_dado);
	if(!no->dado) return F;
	no->tam_dado = tam_dado;
	memcpy(no->dado, dado, tam_dado);
	no->prox = p->topo;
	p->topo = no;
	p->qtd++;
	return V;
}

int pilha_vazia(pilha_t *p){
	return (p->topo == NULL);
}

int desempilha(pilha_t *p, void *dado){
	if(pilha_vazia(p)) return F;
	no_t *aux;
	aux = p->topo;
	memcpy(dado, aux->dado, aux->tam_dado);
	p->topo = p->topo->prox;
	p->qtd--;
	free(aux->dado);
	free(aux);
	return V;	
}

int main(void){
	pilha_t *px;
	px = cria_pilha();
	if(!px) return 0;
	
	int i, ii;
	float f, ff;
	char c, cc, str1[56], str2[56];
	memset(str1, 0, 56);
	memset(str2, 0, 56);
	strcpy(str1, "COM111-abc-123");
	i = 56;
	f = 4.21;
	c = 'x';

	empilha(px, &i, sizeof(int));
	empilha(px, &f, sizeof(float));
	empilha(px, &c, sizeof(char));
	empilha(px, str1, strlen(str1));

	desempilha(px, str2);
	printf("pop %s\n", str2);	

	desempilha(px, &cc);
	printf("pop %c\n", cc);

	desempilha(px, &ff);
	printf("pop %f\n", ff);

	desempilha(px, &ii);
	printf("pop %d\n", ii);

	return 0;
}
