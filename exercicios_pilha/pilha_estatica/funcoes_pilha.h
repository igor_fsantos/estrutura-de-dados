#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct pilha_t{
	void *itens;
	int tam_item;
	int topo;
	int tam_pilha;	
};
	
typedef struct pilha_t pilha_t;

#define VAZIO -1
#define V 1
#define F 0

#ifndef _FUNCOES_PILHA_H
#define _FUNCOES_PILHA_H

void cria_pilha(pilha_t *p, int ti);
int pilha_vazia(pilha_t *p);
int empilha(pilha_t *p, void *dado);
int desempilha(pilha_t *p, void *dado);
void mata_pilha(pilha_t *p);
void imprime_pilha_int(pilha_t *p);
void imprime_pilha_float(pilha_t *p);
pilha_t inverte_pilha(pilha_t *p);
pilha_t copia_pilha(pilha_t *p);

#endif