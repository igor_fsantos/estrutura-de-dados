#include <stdio.h>
#include "funcoes_pilha.h"

int main(int argc, char const *argv[])
{
	if(argc != 2){
		printf("[uso]: %s <tam_pilha>\n", argv[0]);
		return 0;
	}
	int di, i, tam_p;
	pilha_t pi, copia_pi;
	tam_p = atoi(argv[1]);
	cria_pilha(&pi, sizeof(int));
	for(i = 1; i <= tam_p; i++){
		di = i*10;
		if(empilha(&pi, &di)) printf("Empilhou %d\n", di);
		else printf("---Nao empilhou %d\n", di);
	}
	printf("\nCopiando a pilha pi\n");
	copia_pi = copia_pilha(&pi);
	imprime_pilha_int(&copia_pi);
	printf("\nInvertendo a pilha pi\n");
	pi = inverte_pilha(&pi);
	imprime_pilha_int(&pi);
	printf("\nDesempilhando a pilha pi\n");
	while(!pilha_vazia(&pi)){
		printf("------------\n");
		imprime_pilha_int(&pi);
		desempilha(&pi, &di);
		printf("------------\n");
	}
	imprime_pilha_int(&pi);
	mata_pilha(&pi);
	printf("\nDesempilhando a pilha copia_pi\n");
	while(!pilha_vazia(&copia_pi)){
		printf("------------\n");
		imprime_pilha_int(&copia_pi);
		desempilha(&copia_pi, &di);
		printf("------------\n");
	}
	imprime_pilha_int(&copia_pi);
	mata_pilha(&copia_pi);
	return 0;
}