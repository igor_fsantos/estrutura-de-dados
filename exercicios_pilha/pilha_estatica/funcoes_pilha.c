#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "funcoes_pilha.h"

//criacao da pilha 
void cria_pilha(pilha_t *p, int ti){
	p->itens = NULL;
	p->tam_item = ti;
	p->topo = VAZIO;
	p->tam_pilha = 0;
	return;
}
//verifica se a pilha esta vazia
int pilha_vazia(pilha_t *p){
	if(p->topo == VAZIO) return V;	
	return F;
}
//empilhamento
int empilha(pilha_t *p, void *dado){
	p->itens = realloc(p->itens, (p->tam_pilha + 1) * p->tam_item);
	if(p->itens == NULL){
		printf("[erro]: Nao foi possivel alocar a memoria\n");
		 return F;
	}
	p->topo++;
	p->tam_pilha++;
	void *dest;
	dest = p->itens + p->topo * p->tam_item; //itens[topo]
	memcpy(dest, dado, p->tam_item);
	return V;
}
//desempilhamento
int desempilha(pilha_t *p, void *dado){
	if(pilha_vazia(p)) return F;
	void *origem;
	origem = p->itens + p->topo * p->tam_item;
	memcpy(dado, origem, p->tam_item);
	if(p->tam_pilha > 1){
		p->itens = realloc(p->itens, (p->tam_pilha - 1) * p->tam_item);
		if(p->itens == NULL){
			printf("[erro]: Nao foi possivel alocar a memoria\n");
		 	return F;
		}
	}
	p->topo--;
	p->tam_pilha--;	
	return V;
}
//libera a memoria alocada para a pilha 
void mata_pilha(pilha_t *p){
	free(p->itens);
	p->topo = VAZIO;
	p->tam_item = 0;
	p->tam_pilha = 0;
	return;
}
//imprime pilha 
void imprime_pilha_int(pilha_t *p){
	int dado, i;
	for(i = p->topo; i >= 0; i--){
		dado = *(int *)(p->itens + i * p->tam_item);
		printf("Elemento %d\n", dado);
	}
	return;
}
void imprime_pilha_float(pilha_t *p){
	float dado;
	int i;
	for(i = p->topo; i >= 0; i--){
		dado = *(float *)(p->itens + i * p->tam_item);
		printf("Elemento %f\n", dado);
	}
	return;
}
//retorna o tamanho da pilha 
int tamanho_pilha(pilha_t *p){
	return p->tam_pilha;
}
//inverte pilha 
pilha_t inverte_pilha(pilha_t *p){
	pilha_t aux;
	void *origem;
	int i = 0;
	if(origem == NULL){
		printf("[erro]: Nao foi possivel alocar a memoria\n");
		return *(p);
	}
	cria_pilha(&aux, p->tam_item);
	while(aux.topo < p->topo){
		origem = p->itens + (p->topo - i) * p->tam_item;	
		empilha(&aux, origem);
		i++;	
	}
	return aux;
}
//duplica pilha
pilha_t copia_pilha(pilha_t *p){
	pilha_t inv_aux;
	pilha_t aux;
	cria_pilha(&inv_aux, p->tam_item);
	cria_pilha(&aux, p->tam_item);
	inv_aux = inverte_pilha(p);
	aux = inverte_pilha(&inv_aux);
	mata_pilha(&inv_aux);
	return aux;
}