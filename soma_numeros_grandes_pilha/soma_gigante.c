#include <stdio.h>
#include <stdlib.h>
#include "funcoes_soma.h"

int main(int argc, char const *argv[])
{
	if(argc != 3){
		printf("[uso]: %s <num1> <num2>\n", argv[0]);
		return 0;
	}
	pilha_t *pi1, *pi2, *pi_result;
	char *string_pilha;

	pi1 = cria_pilha();
	pi2 = cria_pilha();
	pi_result = cria_pilha();

	printf("Preenchendo as pilhas...\n");
	printf("Valores da pilha pi1:\n");
	preenche_pilha_int(pi1, argv[1]);
	imprime_pilha_int(pi1);

	printf("\nValores da pilha pi2:\n");
	preenche_pilha_int(pi2, argv[2]);
	imprime_pilha_int(pi2);

	printf("\nSomando as pilhas...\n");
	soma_pilhas_int(pi1, pi2, pi_result);
	printf("Pilha resultante:\n");	
	imprime_pilha_int(pi_result);

	printf("\nTransformando os valores da pilha resultante em string...\n");
	string_pilha = pilha_para_string(pi_result);
	printf("String da pilha resultante: %s\n", string_pilha);

	free(string_pilha);
	mata_pilha(pi1);
	mata_pilha(pi2);
	mata_pilha(pi_result);

	return 0;
}