#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define V 1
#define F 0

struct no_t{
	void *dado;
	int tam_dado;
	struct no_t *prox;
};

typedef struct no_t no_t;

struct pilha_t{
	no_t *topo;
	int qtd;
};

typedef struct pilha_t pilha_t;

#ifndef _FUNCOES_SOMA_H
#define	_FUNCOES_SOMA_H

pilha_t *cria_pilha(void);
int empilha(pilha_t *p, void *dado, int tam_dado);
int pilha_vazia(pilha_t *p);
int desempilha(pilha_t *p, void *dado);
void imprime_pilha_int(pilha_t *p);
void preenche_pilha_int(pilha_t *p, const char *str);
void soma_pilhas_int(pilha_t *p1, pilha_t *p2, pilha_t *pi_result);
char *pilha_para_string(pilha_t *p);
void mata_pilha(pilha_t *p);

#endif