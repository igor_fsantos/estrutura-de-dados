#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "funcoes_soma.h"

pilha_t *cria_pilha(void){
	pilha_t *p;
	p = (pilha_t *)calloc(1, sizeof(pilha_t));
	if(!p) return NULL;
	p->topo = NULL;
	p->qtd = 0;
	return p;
}

int empilha(pilha_t *p, void *dado, int tam_dado){
	no_t *no;
	no = (no_t *)calloc(1, sizeof(no_t));
	if(!no) return F;
	no->dado = calloc(1, tam_dado);
	if(!no->dado) return F;
	no->tam_dado = tam_dado;
	memcpy(no->dado, dado, tam_dado);
	no->prox = p->topo;
	p->topo = no;
	p->qtd++;
	return V;
}

int pilha_vazia(pilha_t *p){
	return (p->topo == NULL);
}

int desempilha(pilha_t *p, void *dado){
	if(pilha_vazia(p)) return F;
	no_t *aux;
	aux = p->topo;
	memcpy(dado, aux->dado, aux->tam_dado);
	p->topo = p->topo->prox;
	p->qtd--;
	free(aux->dado);
	free(aux);
	return V;	
}

void imprime_pilha_int(pilha_t *p){
	no_t *aux;
	aux = p->topo;
	while(aux != NULL){
		printf("%d ", *(int *)aux->dado);
		aux = aux->prox;
	}
	printf("\n");
	return;
}

void preenche_pilha_int(pilha_t *p, const char *str){
	char entrada[2];
	int i = 0, numero;
	entrada[1] = '\0';
	while(str[i] != '\0'){
		entrada[0] = str[i];
		numero = atoi(entrada);
		empilha(p, &numero, sizeof(int));
		i++;
	}
}

void soma_pilhas_int(pilha_t *p1, pilha_t *p2, pilha_t *pi_result){
	int dado1, dado2, soma, sobra = 0;
	while((!pilha_vazia(p1)) || (!pilha_vazia(p2))){
		dado1 = dado2 = 0;
		desempilha(p1, &dado1);
		desempilha(p2, &dado2);
		soma = dado1 + dado2 + sobra;
		if(soma >= 10){
			sobra = (soma/10);
			soma = (soma % 10);
		}else sobra = 0;
		empilha(pi_result, &soma, sizeof(int));
	}
	if(sobra) empilha(pi_result, &sobra, sizeof(int));
	return;
}

char *pilha_para_string(pilha_t *p){
	char *str_num;
	int dado, i = 0;
	void *topo;
	topo = (no_t *)calloc(1, sizeof(no_t));
	str_num = (char *)calloc(p->qtd + 1, sizeof(char));
	if((!topo) || (!str_num)){
		printf("[erro]: Nao foi possivel alocar memoria\n");
		exit(0);
	}
	memcpy(topo, p->topo, sizeof(no_t));
	while(i < p->qtd){
		dado = *(int *)p->topo->dado;
		str_num[i] = (char)(((int)'0')+dado);
		p->topo = p->topo->prox;
		i++;
	}
	str_num[i] = '\0';
	p->topo = topo;
	return str_num;
}

void mata_pilha(pilha_t *p){
	no_t *aux;
	while(!pilha_vazia(p)){
		aux = p->topo;
		p->topo = p->topo->prox;
		free(aux->dado);
		free(aux);
	}
	free(p);
	return;
}