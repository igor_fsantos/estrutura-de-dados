#include <stdio.h>

#define STR_LEN 256
#define EMAIL_LEN 64
#define TEL_LEN 32
#define N_ALUNOS 2

struct pessoa_t{
	char nome[STR_LEN];
	char endereco[STR_LEN];
	char email[EMAIL_LEN];
	char telefone[TEL_LEN];
};

typedef struct pessoa_t pessoa_t;

struct aluno_t{
	int mat;
	pessoa_t dados;
	int cod_curso;
	int grade;
	int periodo_atual;
	float coef_geral;
	float coef_sem;
	int ano_ingresso;
};

typedef struct aluno_t aluno_t;

aluno_t cadastra_aluno(void) {
	aluno_t aluno;
	printf("--------------\n");
	printf("Mat: ");
	scanf("%d", &aluno.mat);
	printf("Nome: ");
	scanf(" %[^\n]", aluno.dados.nome);
	printf("Endereco: ");
	scanf(" %[^\n]", aluno.dados.endereco);
	printf("Email: ");
	scanf(" %[^\n]", aluno.dados.email);
	printf("Telefone: ");
	scanf(" %[^\n]", aluno.dados.telefone);
	printf("Cod. Curso: ");
	scanf("%d", &aluno.cod_curso);
	printf("Grade: ");
	scanf("%d", &aluno.grade);
	printf("Periodo Atual: ");
	scanf("%d", &aluno.periodo_atual);	
	printf("Coef. geral: ");
	scanf("%f", &aluno.coef_geral);
	printf("Coef. semestral: ");
	scanf("%f", &aluno.coef_sem);
	printf("Ano ingresso: ");
	scanf("%d", &aluno.ano_ingresso);
	return aluno;
}


void cadastra_turma(int n, aluno_t matriculados[]){
	int i;
	for (i=0; i<n; i++){
		matriculados[i] = cadastra_aluno();
	}
	return;
}

int main(void) {
	aluno_t alunos_matriculados[N_ALUNOS];
	// cadastrar alunos
	cadastra_turma(N_ALUNOS, alunos_matriculados);
	return 0;
}
