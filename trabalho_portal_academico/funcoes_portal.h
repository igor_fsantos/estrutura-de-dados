#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STR_LEN 256
#define EMAIL_LEN 64
#define TEL_LEN 32
#define CODE_LEN 16

#define N_ALUNOS 50 //Numero de alunos que serao cadastrados no portal
#define MAX_ALUNOS 50
#define TOT_DISCIPLINAS 17 

#define V 1
#define F 0

struct pessoa_t{
	char nome[STR_LEN];
	char endereco[STR_LEN];
	char email[EMAIL_LEN];
	char telefone[TEL_LEN];
};

typedef struct pessoa_t pessoa_t;

struct aluno_t{
	int mat;
	pessoa_t dados;
	char cod_curso[STR_LEN];
	int grade;
	int periodo_atual;
	float coef_geral;
	float coef_sem;
	int ano_ingresso;
};

typedef struct aluno_t aluno_t;

struct disciplina_t{
	char codigo[CODE_LEN];
	char nome_disciplina[STR_LEN];
	int teoria;
	int pratica;
	int CH_total;
	int periodo;
};

typedef struct disciplina_t disciplina_t;

struct grade_t{
	disciplina_t grade[TOT_DISCIPLINAS];
};

typedef struct grade_t grade_t;

struct turma_t{
	char mat_alunos[MAX_ALUNOS][CODE_LEN];
	char codigo_disciplina[CODE_LEN];
	int vagas_ocupadas;
};

typedef struct turma_t turma_t;

struct aula_t{
	char dia[4];
	char horario[6];
	char codigo_disciplina[CODE_LEN];
};

typedef struct aula_t aula_t;

#ifndef _FUNCOES_PORTAL_H
#define _FUNCOES_PORTAL_H

aluno_t cadastra_aluno(void);
void cadastra_turma(int numDeAlunos, aluno_t *matriculados);
int busca_por_matricula(int numDeAlunos, int matricula, aluno_t *matriculados);
int busca_por_nome(int numDeAlunos, char *nome, aluno_t *matriculados);
void novos_dados_aluno(aluno_t *matriculado, int indice);
void atualizacao_de_dados(int numDeAlunos, aluno_t *matriculados);
grade_t preenche_periodos(const char *nome_arquivo);
void remove_nova_linha(char *str);
int busca_disciplina(grade_t periodos, const char *codigo);
void disciplinas_periodo(grade_t disciplina);
aula_t *leitura_hor_dia_dis(const char *nome_arquivo, int *tamanho);
void oferta_disciplinas_periodo(grade_t disciplina, aula_t *aula, int tamanho);
void inicia_turma(grade_t disciplinas, turma_t *turma);
int turma_cheia(turma_t turma);
int aluno_matriculado_dis(turma_t turma, const char *matricula);
void cadastra_aluno_dis(grade_t disciplina, turma_t *turma, aluno_t *matriculados);
void imprime_menu(void);

#endif