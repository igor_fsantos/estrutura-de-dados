#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "funcoes_portal.h"

aluno_t cadastra_aluno(void) {
	aluno_t aluno;
	printf("--------------\n");
	printf("Matricula: ");
	scanf("%d", &aluno.mat);
	printf("Nome: ");
	scanf(" %[^\n]", aluno.dados.nome);
	printf("Endereco: ");
	scanf(" %[^\n]", aluno.dados.endereco);
	printf("Email: ");
	scanf(" %[^\n]", aluno.dados.email);
	printf("Telefone: ");
	scanf(" %[^\n]", aluno.dados.telefone);
	printf("Cod. Curso: ");
	scanf(" %[^\n]", aluno.cod_curso);
	printf("Grade: ");
	scanf("%d", &aluno.grade);
	printf("Periodo Atual: ");
	scanf("%d", &aluno.periodo_atual);	
	printf("Coef. geral: ");
	scanf("%f", &aluno.coef_geral);
	printf("Coef. semestral: ");
	scanf("%f", &aluno.coef_sem);
	printf("Ano ingresso: ");
	scanf("%d", &aluno.ano_ingresso);
	return (aluno);
}

void cadastra_turma(int numDeAlunos, aluno_t *matriculados){
	int i;
	for (i = 0; i < numDeAlunos; i++){
		matriculados[i] = cadastra_aluno();
	}
	return;
}

int busca_por_matricula(int numDeAlunos, int matricula, aluno_t *matriculados)
{
    int i;
    for(i= 0; i < numDeAlunos; i++)
    {
        if(matriculados[i].mat == matricula) return i;
    }
    return (-1);
}

int busca_por_nome(int numDeAlunos, char *nome, aluno_t *matriculados)
{
    int i;
    for(i = 0; i < numDeAlunos; i++)
    {
        if(strcmp(matriculados[i].dados.nome, nome) == 0) return i;
    }
    return (-1);
}

void novos_dados_aluno(aluno_t *matriculado, int indice)
{
	printf("Endereco: ");
	scanf(" %[^\n]", matriculado[indice].dados.endereco);
	printf("Telefone: ");
	scanf(" %[^\n]", matriculado[indice].dados.telefone);
	printf("Coef. geral: ");
	scanf("%f", &matriculado[indice].coef_geral);
	printf("Coef. semestral: ");
	scanf("%f", &matriculado[indice].coef_sem);
	printf("Periodo atual: ");
	scanf("%d", &matriculado[indice].periodo_atual);
	printf("Grade: ");
	scanf("%d", &matriculado[indice].grade);
	return;
}

void atualizacao_de_dados(int numDeAlunos, aluno_t *matriculados){
    int mat_aux, indice;
    printf("Informe o numero de matricula:\n");
    scanf("%d", &mat_aux);
    indice = busca_por_matricula(numDeAlunos, mat_aux, matriculados);
    if(indice == -1)
    {
        printf("Matricula nao encontrada\n");
        return;
    }
    else
    {
        novos_dados_aluno(matriculados, indice);
        return;
    }
}

grade_t preenche_periodos(const char *nome_arquivo){
	grade_t periodo;
	int i = 0;
	FILE *arq;
	if(!(arq = fopen(nome_arquivo, "r"))){
		printf("[erro]: Nao foi possivel abrir %s\n", nome_arquivo);
		exit(0);
	}
	while (!feof(arq)){
		fgets(periodo.grade[i].codigo, CODE_LEN, arq);
		remove_nova_linha(periodo.grade[i].codigo);
		fgets(periodo.grade[i].nome_disciplina, STR_LEN, arq);
		remove_nova_linha(periodo.grade[i].nome_disciplina);
		fscanf(arq, "%d\n", &periodo.grade[i].teoria);
		fscanf(arq, "%d\n", &periodo.grade[i].pratica);
		fscanf(arq, "%d\n", &periodo.grade[i].CH_total);
		fscanf(arq, "%d\n", &periodo.grade[i].periodo);
		i++;
	}
	fclose(arq);
	return periodo;
}

void remove_nova_linha(char *str){
	int i = 0;
	while(str[i] != '\n') i++;
	str[i] = '\0';
	return;
}

int busca_disciplina(grade_t disciplina, const char *codigo){
	int i = 0;
	while(i < TOT_DISCIPLINAS){
		if(strcmp(disciplina.grade[i].codigo, codigo) == 0) return i;
		i++;
	}
	return -1;
}

void disciplinas_periodo(grade_t disciplina){
	int i;
	int periodo;
	printf("Entre com o periodo (2, 4 ou 6):\n");
	scanf("%d", &periodo);
	if((periodo != 2) && (periodo != 4) && (periodo != 6)){
		printf("Periodo invalido\n");
		return;
	}
	for(i = 0; disciplina.grade[i].periodo != periodo; i++);
	while(disciplina.grade[i].periodo == periodo){
		printf("Codigo: %s\n", disciplina.grade[i].codigo);
		printf("Disciplina: %s\n", disciplina.grade[i].nome_disciplina);
		printf("Teoria: %d\n", disciplina.grade[i].teoria);
		printf("Pratica: %d\n", disciplina.grade[i].pratica);
		printf("Carga horaria total: %d\n", disciplina.grade[i].CH_total);
		printf("Periodo: %d\n\n", disciplina.grade[i].periodo);
		i++;
	}
	return;
}

/* leitura de horario e dias das disciplinas */
aula_t *leitura_hor_dia_dis(const char *nome_arquivo, int *tamanho)
{
	char nome_dis[19], mesmo_nome[19];
	FILE *arq;
	aula_t *aula = NULL;
	int i = 0;

	if(!(arq = fopen(nome_arquivo, "r"))){
		printf("[erro]: Nao foi possivel abrir %s\n", nome_arquivo);
		exit(0);
	}

	while (!feof(arq)){
		fgets(nome_dis, 19, arq);
		remove_nova_linha(nome_dis);
		if(!strcmp(nome_dis, "")) {
			break;
		}
		if(!strcmp("DISCIPLINA", nome_dis)) {
			aula = (aula_t*)realloc(aula, ++i * sizeof(aula_t));
			if(aula == NULL){
				printf("[erro]: Nao foi possivel alocar memoria\n");
				return NULL;
			}
			fgets(nome_dis, 19, arq);
			remove_nova_linha(nome_dis);
			strcpy(aula[i-1].codigo_disciplina, nome_dis);
			strcpy(mesmo_nome, aula[i-1].codigo_disciplina);
			fgets(nome_dis, 19, arq);
			remove_nova_linha(nome_dis);
			strcpy(aula[i-1].horario, nome_dis);
			fgets(nome_dis, 19, arq);
			remove_nova_linha(nome_dis);
			strcpy(aula[i-1].dia, nome_dis);
		} else {
			aula = (aula_t*)realloc(aula, ++i * sizeof(aula_t));
			if(aula == NULL){
				printf("[erro]: Nao foi possivel alocar memoria\n");
				return NULL;
			}
			strcpy(aula[i-1].codigo_disciplina, mesmo_nome);
			strcpy(aula[i-1].horario, nome_dis);
			fgets(nome_dis, 19, arq);
			remove_nova_linha(nome_dis);
			strcpy(aula[i-1].dia, nome_dis);
		}
	}
	*tamanho = i;
	fclose(arq);
	return aula;
}

void oferta_disciplinas_periodo(grade_t disciplina, aula_t *aula, int tamanho){
	int i, j, periodo;
	printf("Entre com o periodo (2, 4 ou 6):\n");
	scanf("%d", &periodo);
	if((periodo != 2) && (periodo != 4) && (periodo != 6)){
		printf("Periodo invalido\n");
		return;
	}
	for(i = 0; i < TOT_DISCIPLINAS; i++){
		if(disciplina.grade[i].periodo == periodo){
			for(j = 0; j < tamanho; j++){
				if(!strcmp(aula[j].codigo_disciplina, disciplina.grade[i].codigo))
					printf("%s\t%s\t%s\n", aula[j].dia, aula[j].horario, aula[j].codigo_disciplina);
			}
		}
	}
	return;
}

void inicia_turma(grade_t disciplinas, turma_t *turma){
	int i;
	for(i = 0; i < TOT_DISCIPLINAS; i++){
		strcpy(turma[i].codigo_disciplina, disciplinas.grade[i].codigo);
		turma[i].vagas_ocupadas = 0;
	}
	return;
}

int turma_cheia(turma_t turma){
	return (turma.vagas_ocupadas == MAX_ALUNOS);
}

int aluno_matriculado_dis(turma_t turma, const char *matricula){
	int i;
	if(turma.vagas_ocupadas == 0) return F;
	for(i = 0; i < turma.vagas_ocupadas; i++){
		if(!strcmp(turma.mat_alunos[i], matricula)) return V;
	}
	return F;
}

void cadastra_aluno_dis(grade_t disciplina, turma_t *turma, aluno_t *matriculados){
	char char_mat[CODE_LEN], codigo_dis[CODE_LEN];
	int i, matricula, indice_aluno, indice_disciplina;
	printf("Informe a matricula do aluno:\n");
	scanf("%d", &matricula);
	indice_aluno = busca_por_matricula(N_ALUNOS, matricula, matriculados);
	if(indice_aluno == -1){
		printf("Matricula nao encontrada\n");
		return;
	}
	printf("Informe o codigo da disciplina:\n");
	scanf(" %s", codigo_dis);
	indice_disciplina = busca_disciplina(disciplina, codigo_dis);
	if(indice_disciplina == -1){
		printf("O codigo da disciplina nao foi encontrado\n");
		return;
	}
	if(disciplina.grade[indice_disciplina].periodo != matriculados[indice_aluno].periodo_atual){
		printf("A disicplina %s eh do periodo %d\n", disciplina.grade[indice_disciplina].codigo, disciplina.grade[indice_disciplina].periodo);
		printf("O numero de matricula %d eh do periodo %d\n", matriculados[indice_aluno].mat, matriculados[indice_aluno].periodo_atual);
		printf("Nao eh possivel realizar a matricula\n");
		return;
	}
	for(i = 0; i < TOT_DISCIPLINAS; i++){
		if(!strcmp(turma[i].codigo_disciplina, codigo_dis)) break;
	}
	sprintf(char_mat, "%d", matricula);
	if(aluno_matriculado_dis(turma[i], char_mat)){
		printf("O aluno ja esta matriculado nesta disciplina\n");
		return;
	}else if(turma_cheia(turma[i])){
		printf("Nao ha mais vagas para esta disciplina\n");
	}else{
		strcpy(turma[i].mat_alunos[turma[i].vagas_ocupadas], char_mat);
		turma[i].vagas_ocupadas++;
		printf("Matricula realizada com sucesso\n");
	}
	return;	
}

void imprime_menu(void){
	printf("\nEscolha o que deseja fazer pressionando um dos seguintes digitos:\n");
	printf("1. Cadastrar alunos no portal\n");
	printf("2. Atualizar dados de aluno\n");
	printf("3. Ver descricao das disciplinas de um periodo\n");
	printf("4. Ver os horarios das disciplinas de um periodo\n");
	printf("5. Matricular aluno em uma disciplina\n");
	printf("-1. Para finalizar o acesso ao Portal Academico\n");
}