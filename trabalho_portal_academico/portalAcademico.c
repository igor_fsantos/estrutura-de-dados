#include <stdio.h>
#include "funcoes_portal.h"

int main(void) {
	aula_t *aula;
	int tamanho, turma_vazia = V, opcao;
	grade_t disciplina;
	turma_t turma[TOT_DISCIPLINAS];
	aluno_t alunos_matriculados[N_ALUNOS];

	disciplina = preenche_periodos("todos_periodos.txt");
	aula = leitura_hor_dia_dis("horarios.txt", &tamanho);
	inicia_turma(disciplina, turma);
	printf("\n~Bem vindo ao Portal Academico!~\n\n");
	while(V){
		imprime_menu();
		scanf("%d", &opcao);
		if(opcao == 1){
			turma_vazia = F;
			cadastra_turma(N_ALUNOS, alunos_matriculados);
		} else if(opcao == 2){
			if(!turma_vazia){
				atualizacao_de_dados(N_ALUNOS, alunos_matriculados);
			} else printf("Nao ha alunos matriculados\n");
		} else if(opcao == 3){
			disciplinas_periodo(disciplina);
		} else if(opcao == 4){
			oferta_disciplinas_periodo(disciplina, aula, tamanho);
		} else if(opcao == 5){
			if(!turma_vazia){
				cadastra_aluno_dis(disciplina, turma, alunos_matriculados);
			} else printf("Nao ha alunos cadastrados no portal\n");
		} else if(opcao == -1){
			printf("Operacao finalizada!\n");
		 	break;
		} else printf("Opcao invalida\n");
	}
	free(aula);	
	return 0;
}